#include <stdio.h>
#include <stdlib.h>

// 'test' never
int test(void)
{
  char p = 3;
  while (p < 10) {
    while (1) {
      p++;
    }
  }
  return 1;
}

// 'main' never
int main(int argc, char** argv)
{
    return test();
}
