#include <stdio.h>
#include <stdlib.h>

// 'fact' never
int fact(int n)
{
    n++;
    if (n % 2 == 0) {
        return fact(n); // never
    } else {
        return fact(n);
    }
}

// 'main' never
int main(int argc, char** argv)
{
    int var = fact(6);
    printf("86! = %d\n", var);
    return 0;
}
