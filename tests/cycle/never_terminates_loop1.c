#include <stdio.h>
#include <stdlib.h>

// 'test' never
int test(void)
{
  char p = 3;
  while (1) {
    printf("%d\n", p); // never
  }
  return 1;
}

// 'main' never
int main(int argc, char** argv)
{
    return test();
}
