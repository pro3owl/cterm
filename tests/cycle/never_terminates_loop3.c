#include <stdio.h>
#include <stdlib.h>

// 'test' never
int test(void)
{
  char p = 3;
  while (1)
  {
    if (p == 2) { // never
      printf("%d\n", p);
    } else {
      printf("%d\n", p + 1);
    }
  }
  return 1;
}

// 'main' never
int main(int argc, char** argv)
{
    return test();
}
