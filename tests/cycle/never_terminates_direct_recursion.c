#include <stdio.h>
#include <stdlib.h>

// 'fact' never
int fact(int n)
{
    return fact(n); // never
}

// 'main' never
int main(int argc, char** argv)
{
    int var = fact(6);
    printf("86! = %d\n", var);
    return 0;
}
