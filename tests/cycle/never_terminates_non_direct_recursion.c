#include <stdio.h>
#include <stdlib.h>

int fact2(int);
int fact3(int);

// 'fact1' never
int fact1(int n1)
{
    return fact2(n1);
}

// 'fact2' never
int fact2(int n2)
{
    return fact3(n2);
}

// 'fact3' never
int fact3(int n3)
{
    return fact1(n3);
}

// 'main' never
int main(int argc, char** argv)
{
    int var = fact1(6);
    printf("86! = %d\n", var);
    return 0;
}
