#include <stdio.h>

// 'test' always (overflow)
int test()
{
    int i = 10;
    while (i <= 10)
    {
        i--;
    }
    return 0;
}

// 'main' always (overflow)
int main(int argc, char** argv)
{
    return test();
}
