#include <stdio.h>

// 'test' always (overflow)
int test()
{
    int i = 0;
    while (i < 10)
    {
        i--;
        i++;
        i--;
        i++;
        i--;
        i--;
    }
    return 0;
}

// 'main' always (overflow)
int main(int argc, char** argv)
{
    return test();
}
