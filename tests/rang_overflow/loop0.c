#include <stdio.h>

// 'test' always (overflow)
int test()
{
    char i = 20;
    while (i >= 20)
    {
        i--;
    }
    return 0;
}

// 'main' always (overflow)
int main(int argc, char** argv)
{
    return test();
}
