#include <stdio.h>
#include <stdlib.h>

// 'fact1' always
int fact1(int n)
{
    if (n <= 2) return n;
    return n * fact2(n - 1);
}

// 'fact2' always
int fact2(int n)
{
    if (n <= 2) return n;
    return n * fact3(n - 1);
}

// 'fact3' always
int fact3(int n)
{
    if (n <= 2) return n;
    return n * fact1(n - 1);
}

// 'main' always
int main(int argc, char** argv)
{
    int var = fact1(6);
    printf("86! = %d\n", var);
    return 0;
}
