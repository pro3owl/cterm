#include <stdio.h>
#include <stdlib.h>

int n;

int fact1()
{
    if (n <= 2) return n;
    int n_prev = n;
    n--;
    return n_prev * fact1();
}

int fact2()
{
    static int p = 6;
    if (p <= 2) return p;
    int p_prev = p;
    p--;
    return p_prev * fact2();
}

int main(int argc, char** argv)
{
    n = 6;
    int var1 = fact1();
    int var2 = fact2();
    printf("6! = %d\n", var1);
    printf("6! = %d\n", var2);
    return 0;
}
