#include <stdio.h>

// 'test' never
int test(int x)
{
    while (x != 0) // never
    {
        x = -2 * x + 10;
        printf("%d\n", x);
    }
    return 0;
}

// 'main' never
int main(int argc, char** argv)
{
    int x = 6;
    return test(x);
}
