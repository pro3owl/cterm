#include <stdio.h>
#include <stdlib.h>

// 'sum' always
int sum(int x)
{
    if (x <= 0) return 0;
    return x + sum(x - 1);
}

// 'main' always
int main(int argc, char** argv)
{
    int var = sum(6);
    printf("86! = %d\n", var);
    return 0;
}
