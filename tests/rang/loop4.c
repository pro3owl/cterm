#include <stdio.h>

// 'test' always
int test()
{
    int i = 0;
    while (i < 10)
    {
        i--;
        i++;
        i--;
        i++;
        i++;
    }
    return 0;
}

// 'main' always
int main(int argc, char** argv)
{
    return test();
}
