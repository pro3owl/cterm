#include <stdio.h>

// 'test' never
int test()
{
    int i = 0;
    while (i < 10) // never
    {
        i--;
        i++;
        i--;
        i++;
    }
    return 0;
}

// 'main' never
int main(int argc, char** argv)
{
    return test();
}
