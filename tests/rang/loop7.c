#include <stdio.h>

// 'test' always
int test()
{
    int i = 0, j = 0;
    while (i - j >= 1) // terminate: always
    {
        i -= 1;
        j += 2;
    }
    return 0;
}

// 'main' always
int main(int argc, char** argv)
{
    return test();
}
