#include <stdio.h>

// 'test' always
int test(int x)
{
    while (x >= 0)
    {
        x = -2 * x + 10;
    }
    return 0;
}

// 'main' always
int main(int argc, char** argv)
{
    int x = 5;
    return test(x);
}
