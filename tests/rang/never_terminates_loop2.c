#include <stdio.h>
#include <stdlib.h>

// 'test' never
int test(void)
{
  char p = 3;
  while (p != 1) // never
  {
    printf("%d\n", p);
    p = 2;
  }
  return 1;
}

// 'main' never
int main(int argc, char** argv)
{
    return test();
}
