#include <stdio.h>

// 'test' always
int test()
{
    int i = 0;
    while (i < 10)
    {
        if (i > 5) {
            i++;
        } else {
            i += 2;
        }
    }
    return 0;
}

// 'main' always
int main(int argc, char** argv)
{
    return test();
}
