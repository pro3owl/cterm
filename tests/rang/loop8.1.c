#include <stdio.h>

// 'test' always
int test(char Nat, char Pos, char i, char j)
{
    if (Nat >= 0 && Pos >= 1) {
        while (i - j >= 1)
        {
            i -= Nat;
            j += Pos;
        }
    }
    return 0;
}

// 'main' always
int main(int argc, char** argv)
{
    for (char nat = -128; nat != -128; nat++) {
        for (char pos = -128; pos != -128; pos++) {
            for (char i = -128; i != -128; i++) {
                for (char j = -128; j != -128; j++) {
                    printf("test(%d,%d,%d,%d)\n", nat, pos, i, j);
                    test(nat, pos, i, j);
                }
            }
        }
    }
    printf("finished!\n");
}
