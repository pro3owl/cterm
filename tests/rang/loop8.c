#include <stdio.h>

// 'test' always
int test(int Nat, int Pos)
{
    if (Nat >= 0 && Pos >= 1) {
        int i = 0, j = 0;
        while (i - j >= 1)
        {
            i -= Nat;
            j += Pos;
        }
    }
    return 0;
}

// 'main' always
int main(int argc, char** argv)
{
    int nat = 0;
    int pos = 1;
    return test(nat, pos);
}
