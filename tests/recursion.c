#include <stdio.h>
#include <stdlib.h>

int fact(int n)
{
    if (n <= 2) return n;
    return n * fact(n - 1);
}

int main(int argc, char** argv)
{
    int var = fact(6);
    printf("86! = %d\n", var);
    return 0;
}
