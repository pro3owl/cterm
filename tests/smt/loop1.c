#include <stdio.h>

// 'test' never
int test()
{
    char i = 0;
    int var = 1024;
    while (i < var) // never
    {
        i++;
    }
    return 0;
}

// 'main' never
int main(int argc, char** argv)
{
    return test();
}
