#include <stdio.h>

// 'test' never
int test()
{
    char i = 0;
    while (i < 1024) // never
    {
        i++;
    }
    return 0;
}

// 'main' never
int main(int argc, char** argv)
{
    return test();
}
