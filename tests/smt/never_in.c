// 'test' always
int test(void)
{
  char p = 3;
  while (p > 1024) {
    p++;
  }
  return 1;
}

// 'main' never
int main(int argc, char** argv)
{
    return test();
}
