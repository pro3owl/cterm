#include <stdio.h>
#include <stdlib.h>

struct A {

};

//int simpleLoop(void)
//{
//  char p = 3;
//  while (p != 1)
//  {
//    p += 2;
//  }
//  return 1;
//}

int test(void)
{
  char p = 3;
  while (p != 1)
  {
    printf("%d\n", p);
    p += 2;
  }
  return 1;
}

int main(int argc, char** argv)
{
    FILE *fp;
    fp = fopen("test.txt", "r");
    fclose(fp);
    simpleLoop();
    return test();
}
