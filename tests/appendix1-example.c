void func1(int n)
{
    while (n < 100) {
        n++;
    }
}

int func2(int n)
{
    return func2(n - 1);
}

int main(int argc, char** argv)
{
    func1(10);
    return func2(10);
}
