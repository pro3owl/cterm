#!/bin/ruby

require 'optparse'

class String
def black;          "\033[30m#{self}\033[0m" end
def red;            "\033[31m#{self}\033[0m" end
def green;          "\033[32m#{self}\033[0m" end
def brown;          "\033[33m#{self}\033[0m" end
def blue;           "\033[34m#{self}\033[0m" end
def magenta;        "\033[35m#{self}\033[0m" end
def cyan;           "\033[36m#{self}\033[0m" end
def gray;           "\033[37m#{self}\033[0m" end
def bg_black;       "\033[40m#{self}\033[0m" end
def bg_red;         "\033[41m#{self}\033[0m" end
def bg_green;       "\033[42m#{self}\033[0m" end
def bg_brown;       "\033[43m#{self}\033[0m" end
def bg_blue;        "\033[44m#{self}\033[0m" end
def bg_magenta;     "\033[45m#{self}\033[0m" end
def bg_cyan;        "\033[46m#{self}\033[0m" end
def bg_gray;        "\033[47m#{self}\033[0m" end
def bold;           "\033[1m#{self}\033[22m" end
def reverse_color;  "\033[7m#{self}\033[27m" end
end

def retrieve_analysis_output(filename)
    base_name = filename[0..-3]
    bc_file = base_name + '.bc'
    prepare_llvm_cmd = "clang -c -O0 -g -emit-llvm #{filename} -o #{bc_file}"
    #puts prepare_llvm_cmd
    %x( #{prepare_llvm_cmd} )
    convert_to_ll = "llvm-dis #{bc_file}"
    #puts convert_to_ll
    %x( #{convert_to_ll} )
    run_analysis_cmd = "opt -load ./lib/libcterm.so -cterm #{bc_file} -o /dev/null"
    #puts run_analysis_cmd
    output = %x{ #{run_analysis_cmd} }
    return output
end

def clean_up(filename)
    base_name = filename[0..-3]
    bc_file = base_name + '.bc'
    ll_file = base_name + '.ll'
    %x( rm #{bc_file} #{ll_file} )
end

def make_test_from_file(filename)
    puts "running test: #{filename}"
    failed = "[FAILED]".red.bold
    passed = "[PASSED]".green.bold
    unexpected = "[UNEXPECTED]".blue

    expected_terminations = {}
    File.open(filename, "r") do |infile|
        counter = 0
        while (line = infile.gets)
            counter = counter + 1
            if line =~ /\/\/\s*(overflow|never|always)/
                expected_terminations[counter] = $1
            elsif line =~ /\/\/\s*'(\w+)'\s+(overflow|never|always)/
                expected_terminations[$1] = $2
            end
        end
    end

    tests_failed = false
    output = retrieve_analysis_output(filename)
    #puts output
    output.split("\n").each do |line|
        if line =~ /^\[(\d+)\] loop terminates (overflow|never|always) /
            line_number = $1.to_i
            result_termination = $2
            if expected_terminations.has_key?(line_number)
                expected = expected_terminations[line_number]
                if expected == result_termination
                    puts "#{passed} at line #{line_number} termination '#{expected}'"
                    expected_terminations.delete(line_number)
                else
                    tests_failed = true
                    puts "#{failed} at line #{line_number} expected termination '#{expected}' actual result '#{result_termination}'"
                end
            else
                puts "#{unexpected} at line #{line_number} unexpected termination result '#{result_termination}'"
            end
        elsif line =~ /^function\s+'(\w+)'\s+terminates\s+(overflow|never|always)\s*/
            function_name = $1
            result_termination = $2
            if expected_terminations.has_key?(function_name)
                expected = expected_terminations[function_name]
                if expected == result_termination
                    puts "#{passed} for function #{function_name} termination '#{expected}'"
                    expected_terminations.delete(function_name)
                else
                    tests_failed = true
                    puts "#{failed} for function #{function_name} expected termination '#{expected}' actual result '#{result_termination}'"
                end
            else
                puts "#{unexpected} for function #{function_name} unexpected termination result '#{result_termination}'"
            end
        end
    end

    expected_terminations.each do |key, expected|
        if key.is_a? Integer
            line_number = key
            puts "#{failed} at line #{line_number} loop termination not proved '#{expected}'"
        else
            puts "#{failed} function #{key} termination not proved '#{expected}'"
        end
    end

    #if expected_terminations.empty? and not failed
        clean_up(filename)
    #end
end

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: run_tests.rb [directories or files]"
  #opts.on('-d', '--dir DIRECTORY', 'Tests directory name') { |v| options[:dir] = v }
end.parse!

%x( make -j )
status = $?.exitstatus
if status != 0
    abort("bad compile exitstatus: #{status}")
end

test_files = []
ARGV.each do |path|
    if path =~ /.+\.c/
        test_files.push(path)
    else
        test_files += Dir["#{path}/*.c"]
    end
end

if not test_files.empty?
    test_files.each do |test|
        make_test_from_file(test)
    end
    exit
end

abort("no test files or directory specified")

#    for @test_files -> $file {
#        my $relative_path = ~$file;
#        test_from_file($relative_path);
#    }
#}
