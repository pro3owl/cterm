#!/bin/perl6-m

sub MAIN(*@filenames)
{
  my $llvm_file = @filenames.map({$_ ~~ m@(<-[/]>+?)[\.\w+]?\s*$@; $0}).join("-") ~ '.bc';
  #my $prepare_llvm_cmd = "clang -c -O0 -g -emit-llvm @filenames.join(' ') -o $llvm_file";
  my $prepare_llvm_cmd = "clang --std=c99 -c -O0 -gline-tables-only -emit-llvm @filenames.join(' ') -o $llvm_file";
  say $prepare_llvm_cmd;
  shell $prepare_llvm_cmd;
  my $convert_to_ll = "llvm-dis $llvm_file";
  say $convert_to_ll;
  shell $convert_to_ll;
  my $run_analysis_cmd = "make && opt -load ./lib/libcterm.so -cterm $llvm_file";
  say $run_analysis_cmd;
  shell $run_analysis_cmd;
}
