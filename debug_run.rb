#!/bin/ruby

filename = ARGV[0]
if (!filename)
    abort("no file specified")
end
bc_file = filename[0..-3] + '.bc'
prepare_llvm_cmd = "clang -c -O0 -g -emit-llvm #{filename} -o #{bc_file}"
puts prepare_llvm_cmd
%x( #{prepare_llvm_cmd} )
convert_to_ll = "llvm-dis #{bc_file}"
puts convert_to_ll
%x( #{convert_to_ll} )
run_analysis_cmd = "make && opt -load ./lib/libcterm.so -cterm #{bc_file}"
puts run_analysis_cmd
system(run_analysis_cmd)
