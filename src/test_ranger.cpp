//#include "Types.h"
#include "Equation.h"
#include "LPEquationsSolver.h"

// only condition
void test1() {
    typedef Equation<int*> Equation;
    Equation condition;
    int var_i = 1;
    int var_ui = 2;
    // i <= 10
    condition.arguments[&var_i] = -1;
    condition.free_arg = -20;

    LPEquationsSolver<int*> solver;
    bool solution1 = solver.solve({condition});

    assert(solution1 == false);
}

// only update statement
void test2() {
    typedef Equation<int*> Equation;
    Equation update;
    int var_i = 1;
    int var_ui = 2;

    // i' = i + 1 -> -i' + i <= -1
    update.arguments[&var_i] = 1;
    update.state_var = &var_i;
    update.next_state_var.first = &var_ui;
    update.next_state_var.second = -1;
    update.free_arg = -1;

    LPEquationsSolver<int*> solver;
    bool solution1 = solver.solve({update});

    assert(solution1 == false);
}

// one condition, one update -- ok
void test3() {
    typedef Equation<int*> Equation;
    Equation update;
    Equation condition;
    int var_i = 1;
    int var_ui = 2;
    // i <= 20
    condition.arguments[&var_i] = 1;
    condition.free_arg = 20;

    // i' = i + 1 -> -i' + i <= -1
    update.arguments[&var_i] = 1;
    update.state_var = &var_i;
    update.next_state_var.first = &var_ui;
    update.next_state_var.second = -1;
    update.free_arg = -1;

    LPEquationsSolver<int*> solver;
    bool solution1 = solver.solve({condition, update});

    assert(solution1 == true);
}

// one condition, one update -- not ok
void test4() {
    typedef Equation<int*> Equation;
    Equation update;
    Equation condition;
    int var_i = 1;
    int var_ui = 2;
    // i >= 20
    condition.arguments[&var_i] = -1;
    condition.free_arg = -20;

    // i' = i + 1 -> -i' + i <= -1
    update.arguments[&var_i] = 1;
    update.state_var = &var_i;
    update.next_state_var.first = &var_ui;
    update.next_state_var.second = -1;
    update.free_arg = -1;

    LPEquationsSolver<int*> solver;
    bool solution1 = solver.solve({condition, update});

    assert(solution1 == false);
}

// one condition, one update, one undetermined variable in condition -- not ok
void test5() {
    typedef Equation<int*> Equation;
    Equation update;
    Equation condition;
    int var_i = 1;
    int var_ui = 2;
    int var_j = 3;
    // i + j <= 20
    condition.arguments[&var_i] = 1;
    condition.arguments[&var_j] = 1;
    condition.free_arg = 20;

    // i' = i + 1 -> -i' + i <= -1
    update.arguments[&var_i] = 1;
    update.state_var = &var_i;
    update.next_state_var.first = &var_ui;
    update.next_state_var.second = -1;
    update.free_arg = -1;

    LPEquationsSolver<int*> solver;
    bool solution1 = solver.solve({condition, update});

    assert(solution1 == false);
}

// one condition, one update, one undetermined variable in condition -- not ok
void test6() {
    typedef Equation<int*> Equation;
    Equation updatei, updatej;
    Equation condition;
    int var_i = 1;
    int var_ui = 2;
    int var_j = 3;
    int var_uj = 4;
    // i + j <= 20
    condition.arguments[&var_i] = 1;
    condition.arguments[&var_j] = 1;
    condition.free_arg = 20;

    // i' = i + 1 -> -i' + i <= -1
    updatei.arguments[&var_i] = 1;
    updatei.state_var = &var_i;
    updatei.next_state_var.first = &var_ui;
    updatei.next_state_var.second = -1;
    updatei.free_arg = -1;

    // j' = j + 1
    updatej.arguments[&var_j] = 1;
    updatej.state_var = &var_j;
    updatej.next_state_var.first = &var_uj;
    updatej.next_state_var.second = -1;
    updatej.free_arg = -1;

    LPEquationsSolver<int*> solver;
    bool solution1 = solver.solve({condition, updatei, updatej});

    assert(solution1 == true);
}

// one condition, one update, one undetermined variable in condition -- not ok
void test7() {
    typedef Equation<int*> Equation;
    Equation updatei, updatej;
    Equation condition;
    int var_i = 1;
    int var_ui = 2;
    int var_j = 3;
    int var_uj = 4;
    // i + j <= 20
    condition.arguments[&var_i] = 1;
    condition.arguments[&var_j] = 1;
    condition.free_arg = 20;

    // i' = i + 1 -> -i' + i <= -1
    updatei.arguments[&var_i] = 1;
    updatei.state_var = &var_i;
    updatei.next_state_var.first = &var_ui;
    updatei.next_state_var.second = -1;
    updatei.free_arg = -1;

    // j' = j - 1 -> j - j' >= 1 -> -j + j' <= -1
    updatej.arguments[&var_j] = -1;
    updatej.state_var = &var_j;
    updatej.next_state_var.first = &var_uj;
    updatej.next_state_var.second = 1;
    updatej.free_arg = -1;

    LPEquationsSolver<int*> solver;
    bool solution1 = solver.solve({condition, updatei, updatej});

    assert(solution1 == false);
}

// only overflow termination
void test8() {
    typedef Equation<int*> Equation;
    Equation update, update_overflow;
    Equation condition;
    ValuesMapper<int*> mapper;
    int var_i = 1;
    int var_ui = 2;
    // i >= 20 -> -i <= -20
    condition.arguments[&var_i] = -1;
    condition.free_arg = -20;
    std::cout << "condition: " << condition.dump(mapper, "<=") << "\n";

    // i' = i + 1 -> -i' + i <= -1
    update.arguments[&var_i] = 1;
    update.state_var = &var_i;
    update.next_state_var.first = &var_ui;
    update.next_state_var.second = -1;
    update.free_arg = -1;
    std::cout << "update: " << update.dump(mapper, "<=") << "\n";

    // i' = i + 1 - 256 -> i' = i - 255 -> i - i' >= 255 -> -i + i' <= -255
    update_overflow.arguments[&var_i] = -1;
    update_overflow.state_var = &var_i;
    update_overflow.next_state_var.first = &var_ui;
    update_overflow.next_state_var.second = 1;
    update_overflow.free_arg = -255;
    std::cout << "update_overflow: " << update_overflow.dump(mapper, "<=") << "\n";

    LPEquationsSolver<int*> solver;
    bool solution1 = solver.solve({condition, update});
    bool solution2 = solver.solve({condition, update_overflow});

    std::cout << "solution1:" << solution1 << "\n";
    std::cout << "solution2:" << solution2 << "\n";
    assert(solution1 == false);
    assert(solution2 == true);
}

int main(int argc, char** argv) {
    test1();
    std::cout << "test1: OK\n";
    test2();
    std::cout << "test2: OK\n";
    test3();
    std::cout << "test3: OK\n";
    test4();
    std::cout << "test4: OK\n";
    test5();
    std::cout << "test5: OK\n";
    test6();
    std::cout << "test6: OK\n";
    test7();
    std::cout << "test7: OK\n";
    test8();
    std::cout << "test8: OK\n";
    return 0;
}
