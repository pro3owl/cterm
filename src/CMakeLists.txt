cmake_minimum_required(VERSION 2.8.4)

add_library(cterm MODULE main.cpp ../include/Block.h ../include/Slicer.h ../include/TerminationProver.h ../include/RangerProver.h ../include/SymbolicProver.h ../include/NonTerminationProver.h ../include/DependenciesProver.h ../include/NeverReturnsProver.h ../include/GlobalTermProver.h ../include/ValuesMapper.h ../include/Equation.h ../include/InequalitiesSystem.h ../include/ValuesExpressions.h ../include/SMTEquationsSolver.h ../include/LPEquationsSolver.h ../include/EquationBuilder.h)
target_link_libraries(cterm ${llvm_libs} ${jsoncpp_libs} sonolar-shared glpk)

add_executable(ranger test_ranger.cpp)
target_link_libraries(ranger ${jsoncpp_libs} sonolar-shared glpk)
