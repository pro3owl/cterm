#include "Types.h"
#include "ControlFollower.h"

namespace {
class TermPass : public FunctionPass {
private:
    string filename;

public:
    static char ID;

    TermPass() : FunctionPass(ID) {}

    virtual bool doInitialization(Module &module) {
        filename = module.getModuleIdentifier();
        analyzer.on_global_variables(module.getGlobalList());
        return false;
    }

    virtual bool runOnFunction(Function &func) {
        analyzer.on_function(func, filename);
        return false;
    }

    virtual bool doFinalization(Module &module) {
        filename = "";
        analyzer.on_module_end();
        return false;
    }
private:
    ControlFollower analyzer;
};
}

char TermPass::ID = 100;
static RegisterPass<TermPass> X("cterm", "Termination Analysis Pass", false, false);


