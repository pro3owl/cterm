#pragma once

#include "Types.h"
#include "LLVMTypes.h"

struct Location {
private:
    int line_;
    int column_;
    string filename_;

public:
    Location() : line_(-1), column_(-1), filename_()
    {}

    Location(string file) : line_(-1), column_(-1), filename_(file)
    {}

    Location(const DebugLoc& dloc, string file) : line_(dloc.getLine()), column_(dloc.getCol()), filename_(file)
    {}

    int line() const { return line_; }
    int column() const { return column_; }
    string filename() const { return filename_; }
    bool isUnknown() const { return filename_.empty(); }
    bool isDefinition() const { return not isUnknown() and line_ < 0; }

    string str() const {
        if (isDefinition()) return filename_;
        return filename_ + ":" + to_string(line_) + "(" + to_string(column_) + ")";
    }
};
