#pragma once

#include "Types.h"
#include "LLVMTypes.h"
#include "Location.h"

class VariablesScope {
public:
    void add(Value* variable) {
        assert(variables.count(variable) == 0);
        variables.insert(variable);
    }

    const set<Value*>& getVariables() const { return variables; }

private:
    set<Value*> variables;
};

class Block : public enable_shared_from_this<Block> {
public:
    typedef vector<Instruction*> instructions_list;
    typedef vector<shared_ptr<Block>> children_list;

    Block(Value* block_condition = nullptr, bool negate = false)
    : negate_condition(negate),
      is_return_block(false),
      parent(nullptr)
    {
        condition = block_condition;
    }

    Block(const Block& other)
    : instructions(other.instructions),
      negate_condition(other.negate_condition),
      is_return_block(other.is_return_block),
      condition(other.condition),
      parent(nullptr)
    {
    }

    void setCondition(Value* _condition) { condition = _condition; }
    Value* getCondition() const { return condition; }
    bool negateCondition() const { return negate_condition; }

    shared_ptr<Block> getParent() { return parent; }
    const shared_ptr<Block> getParent() const { return parent; }
    void addChild(shared_ptr<Block> child) {
        if (!child->parent) child->parent = shared_from_this();
        children.push_back(child);
    }

    void addAlternativeParent(shared_ptr<Block> altParent, Value* condition, bool negative) {
        alternativeParents.push_back(make_tuple(altParent, condition, negative));
    }
    const vector<tuple<shared_ptr<Block>, Value*, bool>> getAlternativeParents() const { return alternativeParents; }

    children_list getChildren() { return children; }
    const children_list& getChildren() const { return children; }

    void setContainsReturn() { is_return_block = true; }
    bool containsReturn() const { return is_return_block; }

    void addInstruction(Instruction* inst) {
        if (instructions.empty()) {
            const DebugLoc& loc = inst->getDebugLoc();
            updateLocation(loc, location.filename());
        }
        instructions.push_back(inst);
    }
    const instructions_list getInstructions() const { return instructions; }
    const instructions_list::const_iterator begin() const { return instructions.begin(); }
    const instructions_list::const_iterator end() const { return instructions.end(); }
    bool empty() const { return instructions.empty(); }

    bool isNamed() const { return !name.empty();}
    void setName(const string& _name) { name = _name; }
    const string getName() const { return name; }

    void updateLocation(string filename) { location = Location(filename); }
    void updateLocation(const DebugLoc& loc, string filename) { location = Location(loc, filename); }
    const Location getLocation() const { return location; }

    virtual Json::Value dump(std::set<const Block*> previousBlocks = {}) const {
        previousBlocks.insert(this);
        Json::Value root;
        if (isNamed()) {
            root["_name"] = name;
        }
//        root["location"] = location.str();
        if (condition) {
            string cond_str;
            raw_string_ostream cond_s(cond_str);
            condition->print(cond_s);
            root["_condition"] = (negate_condition ? "not (" + cond_str + ")" : cond_str);
        }
        root["_parent"] = (parent ? parent->getName() : "-");
        root["`instructions"] = dump_instructions();
        if (!children.empty()) {
            root["childnodes"] = Json::Value(Json::ValueType::arrayValue);
            for (auto child : children) {
                if (previousBlocks.count(child.get())) continue;
                root["childnodes"].append(child->dump(previousBlocks));
            }
        }
        return root;
    }

    virtual Json::Value dump_instructions() const {
        return dump_instructions(instructions);
    }

    void addVariable(Value* variable) { scope.add(variable); }
    const VariablesScope& getVariablesScope() const { return scope; }

protected:
    static Json::Value dump_instructions(const instructions_list& instructions) {
        Json::Value json = Json::Value(Json::ValueType::arrayValue);
        for (auto inst : instructions) {
            string inst_str;
            raw_string_ostream inst_s(inst_str);
            inst->print(inst_s);
            json.append(inst_str);
        }
        return json;
    }

private:
    Value* condition;
    bool negate_condition;
    bool is_return_block;
    instructions_list instructions;
    shared_ptr<Block> parent;
    vector<tuple<shared_ptr<Block>, Value*, bool>> alternativeParents;
    children_list children;
    Location location;
    VariablesScope scope;

    string name;
};

std::ostream& operator<<(std::ostream& o, const Block& block) {
    return o << block.dump();
}

class CallBlock : public Block {
public:
    CallBlock(shared_ptr<Block> block)
    : Block(), calledBlock(block)
    {}

    template<class T = Block>
    shared_ptr<T> getCalledBlock() { return dynamic_pointer_cast<T>(calledBlock); }

    const Instruction* getCallIstruction() const { return getInstructions().front(); }

    virtual Json::Value dump() const {
        Json::Value root;
        if (calledBlock->isNamed()) {
            root["called function"] = calledBlock->getName();
        }
        root["_parent"] = (getParent() ? getParent()->getName() : "-");
        assert(getChildren().size() == 1);
        root["childnode"] = root["childnodes"].append(getChildren().front()->dump());
        return root;
    }

private:
    shared_ptr<Block> calledBlock;
};

class FunctionBlock : public Block {
public:
    FunctionBlock(string name, Function* raw)
    : Block(nullptr, false),
      is_declaration(false),
      raw_function(raw)
    {
        setName(name);
    }

    void setOnlyDeclaration() { is_declaration = true; }
    bool isOnlyDeclaration() const { return is_declaration; }

    void addCall(shared_ptr<CallBlock> call) { calls.push_back(call); }
    void addOuterCall(shared_ptr<CallBlock> call) { outerCalls.push_back(call); }
    void addCaller(tuple<shared_ptr<CallBlock>, shared_ptr<FunctionBlock>> caller) { callers.push_back(caller); }

    const vector<shared_ptr<CallBlock>> getCalls() const { return calls; }
    const vector<shared_ptr<CallBlock>> getOuterCalls() const { return outerCalls; }
    const vector<tuple<shared_ptr<CallBlock>, shared_ptr<FunctionBlock>>> getCallers() const { return callers; }

    void addArg(Argument* arg) { args.insert(arg); }
    set<Value*> getArgs() { return args; }

    Function* getRawFunction() { return raw_function; }
private:
    // calls to current from current
    vector<shared_ptr<CallBlock>> calls;
    // calls to other functions from current
    vector<shared_ptr<CallBlock>> outerCalls;
    // calls to current function from other functions
    vector<tuple<shared_ptr<CallBlock>, shared_ptr<FunctionBlock>>> callers;

    set<Value*> args;
    Function* raw_function;

    bool is_declaration;
};
