#pragma once

#include "Types.h"
#include "NonTerminationProver.h"

class NeverReturnsProver : public NonTerminationProver {
public:
    tuple<bool, const Location> proveNonTermination(shared_ptr<FunctionBlock> function, map<string, bool>& /*functionTerminates*/) {
        shared_ptr<Block> cycle = findBlocksCycle(function);

        if (cycle) {
            if (cycle == function) {
                // print nothing
            } else {
                std::cout << "[строка:" << cycle->getLocation().line() << "]"
                    << " цикл не завершается (доказано методом поиска циклов в ГПУ)\n";
            }
        }

        return (cycle ? make_tuple(true, cycle->getLocation())
                      : make_tuple(false, function->getLocation()));
    };

    string getName() { return "NeverReturnsProver"; }

private:
    shared_ptr<Block> findBlocksCycle(shared_ptr<Block> root, set<Block*> stack = {}) {
        if (stack.count(root.get())) return nullptr;
        stack.insert(root.get());

        for (auto& alternative_parent : root->getAlternativeParents()) {
            shared_ptr<Block> parent = get<0>(alternative_parent);
            if (parent->getChildren().size() > 1) continue;
            if (alwaysLeedsTo(root, parent))
                return root;
        }

        if (shared_ptr<FunctionBlock> function = dynamic_pointer_cast<FunctionBlock>(root)) {
            auto self_recursion = function->getCalls();
            if (self_recursion.size()) {
                set<Block*> blocks;
                for (auto call : self_recursion) blocks.insert(call.get());
                if (alwaysLeedsTo(root, blocks))
                    return self_recursion.front();
            }
            for (auto& call : function->getOuterCalls()) {
                if (alwaysLeedsTo(root, call)) {
                    shared_ptr<FunctionBlock> caller = call->getCalledBlock<FunctionBlock>();
                    bool result = alwaysLeedsToThroughCalls(caller, function);
                    if (result) return function;
                }
            }
        }

        for (auto& child : root->getChildren()) {
            shared_ptr<Block> child_cycle = findBlocksCycle(child, stack);
            if (child_cycle) return child_cycle;
        }
        return nullptr;
    }

    bool alwaysLeedsTo(shared_ptr<Block> from, shared_ptr<Block> to) {
        set<Block*> blocks = { to.get() };
        return alwaysLeedsTo(from, blocks);
    }

    bool alwaysLeedsTo(shared_ptr<Block> from, const set<Block*>& to, set<Block*> stack = {}) {
        if (stack.count(from.get())) return false;
        stack.insert(from.get());

        if (from->getChildren().empty()) return false;
        if (from->getChildren().size() == 1 && to.count(from->getChildren().front().get()))
            return true;

        bool result = true;
        for (auto& child : from->getChildren()) {
            result = result && alwaysLeedsTo(child, to, stack);
            if (!result) break;
        }
        return result;
    }

    bool alwaysLeedsToThroughCalls(shared_ptr<FunctionBlock> caller, shared_ptr<FunctionBlock> function,
                                   set<FunctionBlock*> stack = {}) {
        if (caller == function) return true;
        if (stack.count(caller.get())) return false;
        stack.insert(caller.get());

        auto outer_calls = caller->getOuterCalls();
        if (outer_calls.empty()) return false;
        if (outer_calls.size() == 1 && outer_calls.front()->getCalledBlock<FunctionBlock>() == function) return true;

        bool result = false;
        for (auto& call : outer_calls) {
            bool direct = alwaysLeedsTo(caller, call);
            if (!direct) break;

            shared_ptr<FunctionBlock> child_caller = call->getCalledBlock<FunctionBlock>();
            bool subresult = alwaysLeedsToThroughCalls(child_caller, function, stack);
            if (subresult) {
                result = true;
                break;
            }
        }
        return result;
    }
};
