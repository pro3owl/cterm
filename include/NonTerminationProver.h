#pragma once

#include "Types.h"
#include "Block.h"

class NonTerminationProver {
public:
    virtual ~NonTerminationProver() {};

    virtual tuple<bool, const Location> proveNonTermination(shared_ptr<FunctionBlock> function,
                                                            map<string, bool>& functionTerminates) = 0;
    virtual string getName() = 0;
};
