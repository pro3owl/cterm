#pragma once

#include <InequalitiesSystem.h>
#include "Types.h"
#include "ToString.h"
#include "ValuesMapper.h"
#include "EquationBuilder.h"
#include "ValuesExpressions.h"

#define NO_OVERFLOW

namespace {
    size_t find_max_bitwidth(const vector<InstructionEquation>& conditions) {
        return 64U;
    }
}
class SMTEquationsSolver {
public:
    bool areConditionsPossible(const vector<InstructionEquation>& updates, const vector<InstructionEquation>& conditions) {
        size_t bitwidth = find_max_bitwidth(conditions);
        ValuesMapper<Value*> mapper;
        ValuesExpressions expr_mapper(bitwidth);
        Solver& solver = expr_mapper.getSolver();
        Expression zero = expr_mapper.getConstantExpression(0);

        // for each update : <=
        for (const auto& inequality : updates) {
            Expression sum = zero;
            for (const auto& arg : inequality.arguments) {
                if (arg.second == 0) continue;
                Expression coef = expr_mapper.getConstantExpression(arg.second);
                sum = solver.make_add(sum, solver.make_mul(coef, expr_mapper.getValueExpression(arg.first)));
            }
            if (inequality.next_state_var.first) {
                Expression coef = expr_mapper.getConstantExpression(inequality.next_state_var.second);
                sum = solver.make_add(sum, solver.make_mul(coef, expr_mapper.getValueExpression(inequality.next_state_var.first)));
            }
            long free_arg = static_cast<long>(inequality.free_arg);
            Expression bcoef = expr_mapper.getConstantExpression(free_arg);
            solver.assert_formula(solver.make_sle(sum, bcoef));
//            std::cout << "update: " << inequality.dump(mapper, "<=") << "\n";
        }

        // and for each condition : >
        for (const auto& inequality : conditions) {
            Expression sum = zero;
            for (const auto& arg : inequality.arguments) {
                if (arg.second == 0) continue;

                Expression coef = expr_mapper.getConstantExpression(arg.second);
                sum = solver.make_add(sum, solver.make_mul(coef, expr_mapper.getValueExpression(arg.first)));
            }
            if (inequality.next_state_var.first) {
                Expression coef = expr_mapper.getConstantExpression(inequality.next_state_var.second);
                sum = solver.make_add(sum, solver.make_mul(coef, expr_mapper.getValueExpression(inequality.next_state_var.first)));
            }
            long free_arg = static_cast<long>(inequality.free_arg);
            Expression bcoef = expr_mapper.getConstantExpression(free_arg);
            solver.assert_formula(solver.make_sgt(sum, bcoef));
//            std::cout << "condition: " << inequality.dump(mapper, ">") << "\n";
            break;
        }

        // and for each variable : max >= var >= min
        auto variables = expr_mapper.getAllValues();
        for (auto& var : variables) {
            if (EquationBuilder::getIntType(var)) {
                Expression evar = expr_mapper.getValueExpression(var);
                auto min = static_cast<int>(EquationBuilder::getMinPossibleValue(var));
                auto max = static_cast<int>(EquationBuilder::getMaxPossibleValue(var));
                Expression emin = expr_mapper.getConstantExpression(min);
                Expression emax = expr_mapper.getConstantExpression(max);
                solver.assert_formula(solver.make_sge(evar, emin));
                solver.assert_formula(solver.make_sle(evar, emax));
//                std::cout << "limits: " << min << " <= " << mapper.getValueName(var) << " <= " << max << "\n";
            }
        }

//        cout << "[areConditionsPossible] Trying to solve...\n";
        auto result = solver.solve();
//        cout << "[areConditionsPossible] result = " << solver_result_to_str(result) << "\n";

        return solver.solve() == SolveResult::SOLVE_RESULT_SAT;
    }

    bool solve(vector<InstructionEquation> equations) {
        size_t bitwidth = 33U;
        ValuesExpressions expr_mapper(bitwidth);
        Solver& solver = expr_mapper.getSolver();
        int length = equations.size();
        Expression zero = expr_mapper.getConstantExpression(0);

        auto a = buildLambdaVector(solver, length, bitwidth, zero);
        auto b = buildLambdaVector(solver, length, bitwidth, zero);

        auto all_variables = extractAllVariables(equations);

        // a * A' = 0
        for (auto &variable : all_variables) {
            Expression suma = zero;
            for (unsigned int i = 0; i < equations.size(); i++) {
                auto& inequality = equations[i];
                auto a_var = a[i];

                int argument = inequality.state_var == variable ? inequality.next_state_var.second : 0;
//                if (i != 0) cout << " + ";
//                cout << argument << " * a" << i;

                Expression acoef = expr_mapper.getConstantExpression(argument);
                Expression a_mul = solver.make_mul(a_var, acoef);
#ifdef NO_OVERFLOW
                solver.assert_formula(solver.make_not(solver.make_smul_ovfl(a_var, acoef)));
                solver.assert_formula(solver.make_not(solver.make_sadd_ovfl(suma, a_var)));
#endif
                suma = solver.make_add(suma, a_mul);
            }
//            cout << "= 0\n";
            solver.assert_formula(solver.make_equal(suma, zero));
        }

        // (a - b) * A = 0
        for (auto &variable : all_variables) {
            Expression suma = zero;
            for (unsigned int i = 0; i < equations.size(); i++) {
                auto& inequality = equations[i];
                auto a_var = a[i];
                auto b_var = b[i];

                auto argument = inequality.arguments[variable];

//                if (i != 0) cout << " + ";
//                cout << "(a" << i << " - b" << i << ") * " << argument;
                if (argument == 0) continue;

                Expression acoef = expr_mapper.getConstantExpression(argument);
                Expression a_minus_b = solver.make_sub(a_var, b_var);
                Expression ab_mul_A = solver.make_mul(a_minus_b, acoef);
#ifdef NO_OVERFLOW
                solver.assert_formula(solver.make_not(solver.make_ssub_ovfl(a_var, b_var)));
                solver.assert_formula(solver.make_not(solver.make_smul_ovfl(a_minus_b, acoef)));
                solver.assert_formula(solver.make_not(solver.make_sadd_ovfl(suma, ab_mul_A)));
#endif
                suma = solver.make_add(suma, ab_mul_A);
            }
//            cout << " = 0\n";
            solver.assert_formula(solver.make_equal(suma, zero));
        }

        // b * (A + A') = 0
        for (auto &variable : all_variables) {
            Expression sumb = zero;
            for (unsigned int i = 0; i < equations.size(); i++) {
                auto& inequality = equations[i];
                auto b_var = b[i];
                auto argument = inequality.arguments[variable];

//                if (i > 0) cout << " + ";
//                cout << "b" << i << " * ";
                Expression acoef = expr_mapper.getConstantExpression(argument);
                if (inequality.state_var == variable) {
//                    cout << "(" << argument << " + " << inequality.next_state_var.second << ")";
                    Expression new_acoef = expr_mapper.getConstantExpression(inequality.next_state_var.second);
#ifdef NO_OVERFLOW
                    solver.assert_formula(solver.make_not(solver.make_sadd_ovfl(acoef, new_acoef)));
#endif
                    acoef = solver.make_add(acoef, new_acoef);
                } else {
//                    cout << argument;
                }

                Expression b_mul_A = solver.make_mul(b_var, acoef);
#ifdef NO_OVERFLOW
                solver.assert_formula(solver.make_not(solver.make_smul_ovfl(b_var, acoef)));
                solver.assert_formula(solver.make_not(solver.make_sadd_ovfl(sumb, b_mul_A)));
#endif
                sumb = solver.make_add(sumb, b_mul_A);
            }
//            cout << " = 0\n";
            solver.assert_formula(solver.make_equal(sumb, zero));
        }

        // b * free_args < 0
        Expression b_free_args_sum = zero;
        for (unsigned int i = 0; i < equations.size(); i++) {
            auto& inequality = equations[i];
            auto b_var = b[i];

//            if (i > 0) cout << " + ";
//            cout << "b" << i << " * " << inequality.free_arg;

            if (inequality.free_arg == 0) continue;

            Expression free_arg_expr = expr_mapper.getConstantExpression(inequality.free_arg);
            Expression b_free_args = solver.make_mul(b_var, free_arg_expr);

#ifdef NO_OVERFLOW
            solver.assert_formula(solver.make_not(solver.make_smul_ovfl(b_var, free_arg_expr)));
            solver.assert_formula(solver.make_not(solver.make_sadd_ovfl(b_free_args_sum, b_free_args)));
#endif
            b_free_args_sum = solver.make_add(b_free_args_sum, b_free_args);
        }
//        cout << " < 0\n";
        solver.assert_formula(solver.make_slt(b_free_args_sum, zero));

//        cout << "Trying to solve...\n";
        auto result = solver.solve();
//        cout << "result = " << solver_result_to_str(result) << "\n";

        return solver.solve() == SolveResult::SOLVE_RESULT_SAT;
    }

    set<Value*> extractAllVariables(const vector<InstructionEquation>& equations) {
        set<Value*> variables;
        for (auto& inequality : equations) {
            for (auto& arg : inequality.arguments) {
                variables.insert(arg.first);
            }
        }
        return variables;
    }

    vector<Expression> buildLambdaVector(Solver& solver, int length, size_t bitwidth, Expression zero) {
        vector<Expression> vars;
        for (int i = 0; i < length; i++) {
            auto var = solver.make_variable(bitwidth, "");
            vars.push_back(var);
            solver.assert_formula(solver.make_sge(var, zero));
//            cout << "(a|b)" << i << " >= 0\n";
        }
        return vars;
    }
};
