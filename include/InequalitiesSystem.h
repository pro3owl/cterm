#pragma once

#include "Types.h"
#include "ToString.h"
#include "ValuesMapper.h"
#include "EquationBuilder.h"
#include "ValuesExpressions.h"

#define NO_OVERFLOW

struct InequalitiesSystem {
    shared_ptr<ValuesMapper<Value*>> mapper;
    vector<InstructionEquation> equations;
    vector<InstructionEquation> conditions;

    InequalitiesSystem(shared_ptr<ValuesMapper<Value*>> values_mapper)
    : mapper(values_mapper)
    {}

    void addEquation(InstructionEquation& equation) {
        equations.push_back(equation);
    }

    void addCondition(InstructionEquation& equation) {
        conditions.push_back(equation);
    }

    void addSystem(const InequalitiesSystem& system) {
        equations.insert(equations.end(), system.equations.begin(), system.equations.end());
        conditions.insert(conditions.end(), system.conditions.begin(), system.conditions.end());
    }

    vector<InstructionEquation> getAllInequalities() {
        vector<InstructionEquation> all = conditions;
        all.insert(all.end(), equations.begin(), equations.end());
        return all;
    }

    std::size_t size() const { return equations.size(); }

    virtual Json::Value dump() const {
        Json::Value root;
        Json::Value equations_json(Json::ValueType::arrayValue);
        Json::Value conditions_json(Json::ValueType::arrayValue);
        for (auto& equation : equations) {
            equations_json.append(equation.dump(*mapper, "<="));
        }
        for (auto& inequality : conditions) {
            conditions_json.append(inequality.dump(*mapper, "<="));
        }
        root["equations"] = equations_json;
        root["conditions"] = conditions_json;
        return root;
    }
};

std::ostream& operator<<(std::ostream& o, const InequalitiesSystem & equations) {
    return o << equations.dump();
}
