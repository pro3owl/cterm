#pragma once

#include "TerminationProver.h"

class SymbolicProver : public TerminationProver {
public:
    bool proveTermination(shared_ptr<FunctionBlock> function, map<string, bool>& functionTerminates) {
        // @todo
        // go through slices?
        buildExpressions(function);
        return false;
    };

    string getName() { return "SymbolicProver"; }

private:
    void buildExpressions(shared_ptr<Block> block, set<Block*> stack = {}) {
        if (stack.count(block.get())) return;
        stack.insert(block.get());

        for (auto inst : block->getInstructions()) {
            buildInstructionExpression(inst);
        }

        for (auto child : block->getChildren()) {
            buildExpressions(child, stack);
        }
    }

    void buildInstructionExpression(Instruction* inst) {
//        if (evaluated_instructions.count(inst)) return;

        for (unsigned int i = 0; i < inst->getNumOperands(); i++) {
            buildConstants(inst->getOperand(i));
        }

        if (LoadInst* load = dyn_cast<LoadInst>(inst)) {
            buildInstructionExpression(load);
        }
        else if (StoreInst* store = dyn_cast<StoreInst>(inst)) {
            buildInstructionExpression(store);
        }
        else if (BinaryOperator* operation = dyn_cast<BinaryOperator>(inst)) {
            buildInstructionExpression(operation);
        }
        else if (UnaryInstruction* unary_operation = dyn_cast<UnaryInstruction>(inst)){
            buildInstructionExpression(unary_operation);
        }
//        else if (CmpInst* compare = dyn_cast<CmpInst>(inst)) {
//            buildInstructionExpression(compare, false);
//        }
    }

    void buildConstants(Value* value) {
        if (ConstantInt* int_const = dyn_cast<ConstantInt>(value)) {
            // @todo: what if the value is unsigned
            cout << int_const << "\n";
//            evaluated_equations[value].free_arg = int_const->getSExtValue();
        }
    }

    void buildInstructionExpression(LoadInst* load) {
        auto loaded = load->getOperand(0);
        cout << loaded << " loaded to " << load << "\n";
    }

    void buildInstructionExpression(StoreInst* store) {
        Value* what = store->getOperand(0);
        Value* where = store->getOperand(1);
        cout << what << " stored to " << where << "\n";
    }

    void buildInstructionExpression(BinaryOperator* operation) {
        Value* argument1 = operation->getOperand(0);
        Value* argument2 = operation->getOperand(1);

        switch(operation->getOpcode()) {
            case Instruction::Mul:
                cout << argument1 << " MUL " << argument2 << "\n";
                break;
            case Instruction::Add:
                cout << argument1 << " ADD " << argument2 << "\n";
                break;
            case Instruction::Sub:
                cout << argument1 << " SUB " << argument2 << "\n";
                break;
            default:
                cerr << "Unanalyzable instruction found: " << operation->getOpcodeName() << "\n";
                throw std::runtime_error("Unanalyzable instruction");
        }
    }

    void buildInstructionExpression(UnaryInstruction* operation) {
        Value* argument = operation->getOperand(0);
        switch(operation->getOpcode()) {
            case Instruction::SExt: // signed extend
                cout << "SignedExtend " << argument << "\n";
                break;
            case Instruction::Alloca:
                break;
            default:
                cerr << "Unanalyzable instruction found: " << operation->getOpcodeName() << "\n";
                throw std::runtime_error("Unanalyzable instruction");
        }
    }

    void buildInstructionExpression(CmpInst* compare, bool negative) {
//        if (evaluated_expressions.count(compare)) return;

        for (unsigned int i = 0; i < compare->getNumOperands(); i++) {
            buildConstants(compare->getOperand(i));
        }

        Value* argument1 = compare->getOperand(0);
        Value* argument2 = compare->getOperand(1);

        switch((negative ? compare->getInversePredicate() : compare->getPredicate())) {
            case CmpInst::ICMP_ULE:
            case CmpInst::ICMP_SLE: {
                cout << argument1 << " <= " << argument2 << "\n";
                break;
            }
            case CmpInst::ICMP_ULT:
            case CmpInst::ICMP_SLT: {
                cout << argument1 << " < " << argument2 << "\n";
                break;
            }
            case CmpInst::ICMP_UGE:
            case CmpInst::ICMP_SGE: {
                cout << argument1 << " >= " << argument2 << "\n";
                break;
            }
            case CmpInst::ICMP_UGT:
            case CmpInst::ICMP_SGT: {
                cout << argument1 << " > " << argument2 << "\n";
                break;
            }
            case CmpInst::ICMP_EQ: {
                cout << argument1 << " == " << argument2 << "\n";
                break;
            };
            case CmpInst::ICMP_NE: {
                cout << argument1 << " != " << argument2 << "\n";
                break;
            };
            default:
                cerr << "Unanalyzable instruction found: " << compare->getPredicate() << "\n";
                throw std::runtime_error("Unanalyzable instruction");
        }
    }

};
