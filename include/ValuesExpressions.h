#pragma once

#include "Types.h"
#include "LLVMTypes.h"

class ValuesExpressions {
    Solver solver;
    const unsigned int bitwidth;
    map<Value*, Expression> expressions;
public:
    ValuesExpressions(unsigned int bits) : bitwidth(bits) {
    }

    Solver& getSolver() { return solver; }

    std::set<Value*> getAllValues() const {
        std::set<Value*> values;
        for (const auto& pair : expressions) {
            values.insert(pair.first);
        }
        return values;
    }

    // @todo: signed and unsigned?
    // @todo: every variable in expression should have the same bitwidth, if current's less -> add constraints
    Expression getValueExpression(Value* value) {
        if (expressions.count(value)) return expressions[value];

        Expression expr = solver.make_variable(bitwidth, /*getValueName(value).c_str()*/"");
        expressions[value] = expr;
        return expr;
    }

    template <typename T>
    Expression getConstantExpression(T value) {
        Expression small_const = solver.make_constant(value);

        size_t actual_bitwidth;
        solver.get_bitwidth(small_const, actual_bitwidth);
//        std::cout << "evar bitwidth old: " << actual_bitwidth << "\n";
        if (actual_bitwidth != bitwidth) {
            auto evar = solver.make_sign_extend(small_const, bitwidth - actual_bitwidth);

            solver.get_bitwidth(evar, actual_bitwidth);
//            std::cout << "evar bitwidth: " << actual_bitwidth << "\n";
            return evar;
        } else {
            return small_const;
        }
    }

};
