#pragma once

#include <llvm/Pass.h>
#include <llvm/PassAnalysisSupport.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Dominators.h>
#include <llvm/IR/Constants.h>
#include <llvm/Analysis/CFG.h>
#include <llvm/Analysis/PostDominators.h>
#include <llvm/Analysis/CallGraphSCCPass.h>
#include <llvm/Support/raw_ostream.h>

//using namespace llvm;
using llvm::BasicBlock;
using llvm::CmpInst;
using llvm::BinaryOperator;
using llvm::UnaryInstruction;
using llvm::AllocaInst;
using llvm::StoreInst;
using llvm::LoadInst;
using llvm::Value;
using llvm::raw_string_ostream;
using llvm::DebugLoc;
using llvm::Instruction;
using llvm::dyn_cast;
using llvm::isa;
using llvm::CallInst;
using llvm::cast;
using llvm::Module;
using llvm::FunctionPass;
using llvm::ReturnInst;
using llvm::BranchInst;
using llvm::Function;
using llvm::RegisterPass;
using llvm::Constant;
using llvm::ConstantInt;
using llvm::PointerType;
using llvm::Argument;
using llvm::Type;
using llvm::IntegerType;

std::ostream& operator<<(std::ostream& o, Value* value) {
    if (value) {
        string inst_str;
        raw_string_ostream inst_s(inst_str);
        value->print(inst_s);
        return o << inst_str;
    } else {
        return o << "null";
    }
}

std::ostream& operator<<(std::ostream& o, Type* type) {
    if (type) {
        string inst_str;
        raw_string_ostream inst_s(inst_str);
        type->print(inst_s);
        return o << inst_str;
    } else {
        return o << "null";
    }
}

#define USE_LLVM
