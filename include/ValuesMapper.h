#pragma once

#include "Types.h"

template <typename ValueType>
class ValuesMapper {
public:
    ValuesMapper() : counter(0) {}

    bool hasValue(ValueType val) {
        return ids.count(val);
    }

    int getValueId(ValueType val) const {
        if (ids.count(val)) return ids[val];
        counter++;
        ids[val] = counter;
        values[counter] = val;
        return counter;
    }

    string getValueName(ValueType val) const {
        return "$" + to_string(getValueId(val));
    }

    ValueType getValue(int id) const {
        if (values.count(id)) return values[id];
        return nullptr;
    }

    void clear() {
        ids.clear();
        values.clear();
    }

private:
    mutable map<ValueType, int> ids;
    mutable map<int, ValueType> values;
    mutable int counter;
};

#ifdef USE_LLVM
template <>
string ValuesMapper<Value*>::getValueName(Value* val) const {
    if (val->hasName()) {
        return val->getName().str();
    } else {
        return "$" + to_string(getValueId(val));
    }
}
#endif
