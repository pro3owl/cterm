#pragma once

#include <SMTEquationsSolver.h>
#include "TerminationProver.h"
#include "ValuesMapper.h"
#include "Equation.h"
#include "EquationBuilder.h"
#include "InequalitiesSystem.h"
#include "LPEquationsSolver.h"

//#define DEBUG_RANGER

class RangerProver : public TerminationProver {
public:
    RangerProver()
    : mapper(make_shared<ValuesMapper<Value*>>())
    {}

    string getName() { return "RangerProver"; }

    bool proveTermination(shared_ptr<FunctionBlock> function, map<string, bool>& functionTerminates) {
        current_function = function;

        Slicer slicer;
        auto slices = slicer.makeSlices(function);
        if (slices.empty()) return true;

#ifdef DEBUG_RANGER
//        cout << "function args: " ;
//        for (auto arg : function->getArgs()) cout << arg << ", ";
//        cout << "--\n";
#endif

        Slicer::dependencies_map deps = slicer.getVariablesDependencies();
        bool terminates = true;
        auto solver = LPEquationsSolver<Value*>();
        auto checker = SMTEquationsSolver();
        for (auto& slice : slices) {
#ifdef DEBUG_RANGER
            cout << "make equations for block: " << *slice.getRoot() << "\n";
#endif
            auto updates = buildEquationsSystem(slice.getRoot(), deps);
            if (!updates.equations.empty()) {
                auto conditions = buildConditionsInequalities(slice.getRoot());

                auto equations = updates;
                equations.addSystem(conditions);
                assert(equations.size() >= updates.size());
#ifdef DEBUG_RANGER
                std::cout << "equations: " << equations << "\n";
#endif
                if (checker.areConditionsPossible(equations.equations, equations.conditions)) {
                    terminates = solver.solve(equations.getAllInequalities());
                    if (!terminates) {
                        terminates = tryOverflow(equations);
                    }
                } else {
                    terminates = false;
                }
            } else {
                terminates = false;
            }

            if (!terminates) {
                if (slice.isLoop()) {
                    std::cout << "[строка:" << slice.getRoot()->getLocation().line() << "]"
                        << " цикл не завершается (доказано методом линейных ранговых функций)\n";
                } else {
                    std::cout << "[строка:" << slice.getRoot()->getLocation().line() << "]"
                    << " рекурсивный вызов не завершается (доказано методом линейных ранговых функций)\n";
                }
                break;
            }
        }

        current_function.reset();
        mapper->clear();
        evaluated_equations.clear();

        return terminates;
    };

private:
    InequalitiesSystem buildEquationsSystem(shared_ptr<Block> block, Slicer::dependencies_map& deps) {
        buildAllEquations(block);
        vector<pair<StoreInst*, shared_ptr<Block>>> stores;

        InequalitiesSystem equations(mapper);

#ifdef DEBUG_RANGER
        for (auto pair : evaluated_equations)
            cout << " | " << pair.first << ": " << pair.second.dump(*mapper) << "\n";
#endif

        for (auto& mutation : mutations) {
#ifdef DEBUG_RANGER
            cout << "mutation: " << mutation.first << " -> " << mutation.second << "\n";
            cout << "evaluated_equations[mutation.second]: " << evaluated_equations[mutation.second].dump(*mapper) << "\n";
            cout << "evaluated_equations[mutation.first]: " << evaluated_equations[mutation.first].dump(*mapper) << "\n";
#endif
            auto inequalities = EquationBuilder::makeMutationInequality(evaluated_equations[mutation.second],
                                                                        mutation.first, mutation.second, mapper);
            for (auto inequality : inequalities) {
                equations.addEquation(inequality);
            }
        }

        return equations;
    }

    void buildAllEquations(shared_ptr<Block> block, set<Block*> stack = {}) {
        if (stack.count(block.get())) return;
        stack.insert(block.get());

        for (auto inst : block->getInstructions()) {
            buildInstructionEquation(inst);
        }

        for (auto child : block->getChildren()) {
            buildAllEquations(child, stack);
        }
    }

    void buildInstructionEquation(Instruction* inst) {
        if (evaluated_equations.count(inst)) return;

        for (unsigned int i = 0; i < inst->getNumOperands(); i++) {
            buildConstants(inst->getOperand(i));
        }

        if (LoadInst* load = dyn_cast<LoadInst>(inst)) {
            buildInstructionEquation(load);
        }
        else if (StoreInst* store = dyn_cast<StoreInst>(inst)) {
            buildInstructionEquation(store);
        }
        else if (BinaryOperator* operation = dyn_cast<BinaryOperator>(inst)) {
            buildInstructionEquation(operation);
        }
        else if (UnaryInstruction* unary_operation = dyn_cast<UnaryInstruction>(inst)){
            buildInstructionEquation(unary_operation);
        }
//        else if (CmpInst* compare = dyn_cast<CmpInst>(inst)) {
//            buildInstructionEquation(compare, false);
//        }
    }

    void buildConstants(Value* value) {
        if (ConstantInt* int_const = dyn_cast<ConstantInt>(value)) {
            // @todo: what if the value is unsigned
            evaluated_equations[value].free_arg = int_const->getSExtValue();
        }
    }

    void buildInstructionEquation(LoadInst* load) {
        Equation<Value*> equation;
        auto loaded = load->getOperand(0);
        if (evaluated_equations.count(loaded) || mutations.count(loaded)) {
            if (mutations.count(loaded))
                equation.clone(evaluated_equations[mutations[loaded]]);
            else
                equation.clone(evaluated_equations[loaded]);
        } else {
            equation.free_arg = 0;
            equation.arguments[loaded] = 1;
        }
        evaluated_equations[load] = equation;
    }

    void buildInstructionEquation(StoreInst* store) {
        Equation<Value*> equation;
        Value* what = store->getOperand(0);
        Value* where = store->getOperand(1);
        if (!evaluated_equations.count(what)) {
            if (current_function->getArgs().count(what)) {
                evaluated_equations[what] = EquationBuilder::makeVar(what);
            } else {
                cerr << "Cant process operation: arguments are not processed for: " << store << "\n";
                throw std::runtime_error("arguments are not processed");
            }
        }

        if (mutations.count(what))
            equation.clone(evaluated_equations[mutations[what]]);
        else
            equation.clone(evaluated_equations[what]);

        mutations[where] = store;

        evaluated_equations[store] = equation;
    }

    void buildInstructionEquation(BinaryOperator* operation) {
        Equation<Value*> equation;
        Value* argument1 = operation->getOperand(0);
        Value* argument2 = operation->getOperand(1);

        if (!evaluated_equations.count(argument1) || !evaluated_equations.count(argument2)) {
            cerr << "Cant process operation: arguments are not processed for: " << operation << "\n";
            throw std::runtime_error("arguments are not processed");
        }

        auto eq1 = mutations.count(argument1) ? evaluated_equations[mutations[argument1]] : evaluated_equations[argument1];
        auto eq2 = mutations.count(argument2) ? evaluated_equations[mutations[argument2]] : evaluated_equations[argument2];

        equation.clone(eq1);
        switch(operation->getOpcode()) {
            case Instruction::Mul:
                equation *= eq2;
                break;
            case Instruction::Add:
                equation += eq2;
                break;
            case Instruction::Sub:
                equation -= eq2;
                break;
            default:
                cerr << "Unanalyzable instruction found: " << operation->getOpcodeName() << "\n";
                throw std::runtime_error("Unanalyzable instruction");
        }
        evaluated_equations[operation] = equation;
    }

    void buildInstructionEquation(UnaryInstruction* operation) {
        Equation<Value*> equation;
        Value* argument = operation->getOperand(0);

        if (!evaluated_equations.count(argument)) {
            cerr << "Cant process operation: arguments are not processed for: " << operation << "\n";
            throw std::runtime_error("arguments are not processed");
        }

        auto eq = mutations.count(argument) ? evaluated_equations[mutations[argument]] : evaluated_equations[argument];

        switch(operation->getOpcode()) {
            case Instruction::SExt: // signed extend
                equation = eq;
                break;
            case Instruction::Alloca:
                break;
            case Instruction::Trunc:
                // @todo: overflow checks
                break;
            default:
                cerr << operation << "\n";
                cerr << "Unanalyzable instruction found: " << operation->getOpcodeName() << "\n";
                throw std::runtime_error("Unanalyzable instruction");
        }
        evaluated_equations[operation] = equation;
    }

    void buildInstructionEquation(CmpInst* compare, bool negative) {
        if (evaluated_equations.count(compare)) return;

        for (unsigned int i = 0; i < compare->getNumOperands(); i++) {
            buildConstants(compare->getOperand(i));
        }

        Equation<Value*> equation;
        Value* argument1 = compare->getOperand(0);
        Value* argument2 = compare->getOperand(1);

        if (!evaluated_equations.count(argument1) || !evaluated_equations.count(argument2)) {
            cerr << "Cant process operation: arguments are not processed for: " << compare << "\n";
            throw std::runtime_error("arguments are not processed");
        }

        auto eq1 = mutations.count(argument1) ? evaluated_equations[mutations[argument1]] : evaluated_equations[argument1];
        auto eq2 = mutations.count(argument2) ? evaluated_equations[mutations[argument2]] : evaluated_equations[argument2];

        switch((negative ? compare->getInversePredicate() : compare->getPredicate())) {
            case CmpInst::ICMP_ULE:
            case CmpInst::ICMP_SLE: {
                equation = eq1.makeLessThan(eq2);
                break;
            }
            case CmpInst::ICMP_ULT:
            case CmpInst::ICMP_SLT: {
                auto than = eq2;
                than.free_arg -= 1;
                equation = eq1.makeLessThan(than);
                break;
            }
            case CmpInst::ICMP_UGE:
            case CmpInst::ICMP_SGE: {
                equation = eq2.makeLessThan(eq1);
                break;
            }
            case CmpInst::ICMP_UGT:
            case CmpInst::ICMP_SGT: {
                auto than = eq1;
                than.free_arg -= 1;
                equation = eq2.makeLessThan(than);
                break;
            }
            case CmpInst::ICMP_EQ: {
                equation = eq1.makeLessThan(eq2);
                equation.next_equation = make_shared<Equation<Value*>>(eq2.makeLessThan(eq1));
                break;
            };
            case CmpInst::ICMP_NE: {
                auto less_than = eq2;
                less_than.free_arg -= 1;
                equation = eq1.makeLessThan(less_than);

                auto greater_than = eq1;
                greater_than.free_arg -= 1;
                greater_than = eq2.makeLessThan(greater_than);
                equation = greater_than;

                // @todo: generate several systems
//                equation.next_equation = make_shared<Equation<Value*>>(eq2.makeLessThan(greater_than));
                break;
            };
            default:
                cerr << "Unanalyzable instruction found: " << compare->getPredicate() << "\n";
                throw std::runtime_error("Unanalyzable instruction");
        }
        evaluated_equations[compare] = equation;
    }

    InequalitiesSystem buildConditionsInequalities(shared_ptr<Block> block, set<Block*> stack = {}) {
        InequalitiesSystem inequalities(mapper);

        if (stack.count(block.get())) return inequalities;
        stack.insert(block.get());

        if (block->getCondition()) {
            if (CmpInst* inst = dyn_cast<CmpInst>(block->getCondition())) {
                buildInstructionEquation(inst, block->negateCondition());
                auto base_inequality = evaluated_equations[inst];
                if (base_inequality.next_equation)
                    std::cerr << "Next equation skipped: " << base_inequality.next_equation->dump(*mapper, "<=") << "\n";

                inequalities.addCondition(base_inequality);
            } else {
                cerr << "Block condition is not compare instruction\n";
            }
        }

        for (auto child : block->getChildren()) {
            inequalities.addSystem(buildConditionsInequalities(child, stack));
        }
        return inequalities;
    }

    bool tryOverflow(InequalitiesSystem equations) {
        auto solver = LPEquationsSolver<Value*>();
        for (unsigned i = 0; i < equations.equations.size(); i++) {
            if (!equations.equations[i].state_var) continue;

            {
                // positive overflow
                auto system = equations;
                system.equations[i] = EquationBuilder::makePositiveOverflow(system.equations[i]);
#ifdef DEBUG_RANGER
                std::cout << "trying to solve: " << system.dump() << "\n";
#endif
                if (solver.solve(system.getAllInequalities())) {
                    std::cout << "доказана завершаемость при условии положительного переполнения при изменении переменной: "
                    << mapper->getValueName(equations.equations[i].state_var)
//                    << equations.equations[i].dump(*mapper, "<=")
                    << "\n";
                    return true;
                }
            }

            {
                // negative overflow
                auto system = equations;
                system.equations[i] = EquationBuilder::makeNegativeOverflow(system.equations[i]);
#ifdef DEBUG_RANGER
                std::cout << "trying to solve: " << system.dump() << "\n";
#endif
                if (solver.solve(system.getAllInequalities())) {
                    std::cout << "доказана завершаемости при условии отрицательного переполнения при изменении переменной: "
                    << mapper->getValueName(equations.equations[i].state_var)
                    //                    << equations.equations[i].dump(*mapper, "<=")
                    << "\n";
                    return true;
                }
            }
        }
        return false;
    }

private:
    shared_ptr<ValuesMapper<Value*>> mapper;
    map<Value*, Equation<Value*>> evaluated_equations;
    map<Value*, Value*> mutations;
    shared_ptr<FunctionBlock> current_function;
};
