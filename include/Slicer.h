#pragma once

#include "Types.h"
#include "Block.h"

#define DEBUG_SLICER false

class Slice {
public:
    Slice(shared_ptr<Block> block, std::set<Value*> vars = {})
    : root(block), variables(vars), is_recursion(false)
    {}

    shared_ptr<Block> getRoot() { return root; }
    set<Value*> getVars() { return variables; }

    Slice& setRecursion() {
        is_recursion = true;
        return *this;
    }

    bool isRecursion() { return is_recursion; }
    bool isLoop() { return not is_recursion; }

private:
    shared_ptr<Block> root;
    set<Value*> variables;
    bool is_recursion;
};

class Slicer {
public:
    typedef set<Value*> value_dependencies;
    typedef map<Value*, value_dependencies> dependencies_map;

    vector<Slice> makeSlices(shared_ptr<FunctionBlock> root) {
        find_dependencies(root);
        if (DEBUG_SLICER) {
            printVariableDependencies(std::cout);
        }

        vector<Slice> cycles;
        extractLoopSlices(root, cycles);
        extractRecursiveCallsSlices(root, root->getName(), cycles);

        return cycles;
    }

    dependencies_map getVariablesDependencies() {
        return variables_deps;
    }

    dependencies_map getVariablesDependents() {
        return dependent_variables;
    }

protected:
    void extractLoopSlices(shared_ptr<Block> root, vector<Slice>& slices) {
        for (auto& child: root->getChildren()) {
            auto& loopBacks = child->getAlternativeParents();
            if (loopBacks.size() > 0) {
                for (auto& loop : loopBacks) {
                    shared_ptr<Block> block;
                    Value* condition;
                    bool negative;
                    tie(block, condition, negative) = loop;
                    auto loop_end = findActualParentBlock(block, child);
                    std::set<Value*> deps;
                    auto sliced_loop_end = getNearestLoop(loop_end, child, deps);
                    auto loop_head = getPassHead(sliced_loop_end);
                    loop_head->setCondition(condition);
                    slices.push_back(Slice(loop_head));
                    if (DEBUG_SLICER)
                        std::cout << "Found loop: \n" << loop_head->dump() << "\n";
                }
            } else {
                extractLoopSlices(child, slices);
            }
        }
    }

    shared_ptr<Block> findActualParentBlock(shared_ptr<Block> block, const shared_ptr<Block> target_child) {
        for (auto &child : block->getChildren()) {
            if (child == target_child) {
                return block;
            }
        }
        for (auto &child : block->getChildren()) {
            shared_ptr<Block> nearest_block = findActualParentBlock(child, target_child);
            if (nearest_block) return nearest_block;
        }
        return nullptr;
    }

    shared_ptr<Block> getNearestLoop(shared_ptr<Block> block, const shared_ptr<Block> loop_begin,
                                     std::set<Value*>& variables) {
        assert(block);
        if (DEBUG_SLICER)
            std::cout << "[getNearestLoop] current block:\n" << block->dump_instructions() << "\n";
        if (block == loop_begin) {
            return sliceBlockByVariables(block, variables);
        }

        auto parent = block->getParent();
        if (!parent) return nullptr;

        std::set<Value*> current_deps = extractDependenciesFromCondition(block);
        variables.insert(current_deps.begin(), current_deps.end());

        auto parent_block = getNearestLoop(parent, loop_begin, variables);
        if (!parent_block) return nullptr;

        auto current = sliceBlockByVariables(block, variables);
        if (current->empty() && !current->getCondition()) {
            for (auto child: current->getChildren()) {
                parent_block->addChild(child);
            }
            return parent_block;
        } else {
            parent_block->addChild(current);
            return current;
        }
    }

    void extractRecursiveCallsSlices(shared_ptr<FunctionBlock> root, const string& function_name, vector<Slice>& slices,
                                     std::set<FunctionBlock*> analyzed_blocks = {}) {
        if (analyzed_blocks.count(root.get())) return;
        analyzed_blocks.insert(root.get());

        if (root->getName() == function_name) {
            for (auto &call : root->getCalls()) {
                set<Value*> variables = extractCallVariables(call);
                auto raw_slice = getNearestRecursion(call, function_name, variables);
                assert(raw_slice);
                auto slice_head = getPassHead(raw_slice);
                if (DEBUG_SLICER)
                    std::cout << "Found recursive call to " << function_name << ": \n" << *slice_head << "\n------\n";
                auto callArgs = call->getVariablesScope().getVariables();
                slices.push_back(Slice(slice_head, callArgs).setRecursion());
            }
        }

        for (auto& caller_info : root->getCallers()) {
            shared_ptr<CallBlock> call;
            shared_ptr<FunctionBlock> caller;
            tie(call, caller) = caller_info;
            if (caller->getName() == function_name) {
                set<Value*> variables = extractCallVariables(call);
                auto raw_slice = getNearestRecursion(call, function_name, variables);
                assert(raw_slice);
                auto slice_head = getPassHead(raw_slice);
                if (DEBUG_SLICER)
                    std::cout << "Found non-direct recursive call to " << root->getName()
                        << " from " << function_name << ": \n" << *slice_head << "\n------\n";
                auto callArgs = call->getVariablesScope().getVariables();
                slices.push_back(Slice(slice_head, callArgs).setRecursion());
            } else {
                extractRecursiveCallsSlices(caller, function_name, slices, analyzed_blocks);
            }
        }
    }

    set<Value*> extractCallVariables(shared_ptr<CallBlock> call) {
        set<Value*> variables;
        auto callArgs = call->getVariablesScope().getVariables();
        for (auto argVar : callArgs) {
            auto& dependencies = variables_deps[argVar];
            variables.insert(dependencies.begin(), dependencies.end());
            variables.insert(argVar);
        }
        return variables;
    }

    shared_ptr<Block> getNearestRecursion(shared_ptr<Block> block, const string& recursiveFunction,
                                          set<Value*>& variables) {

        set<Value*> blockVariables = extractDependenciesFromCondition(block);
        variables.insert(blockVariables.begin(), blockVariables.end());
        auto slice = sliceBlockByVariables(block, variables);

        if (block->getName() == recursiveFunction) {
            return slice;
        }

        auto parent = block->getParent();
        if (!parent)
            return nullptr;

        auto parentSlice = getNearestRecursion(parent, recursiveFunction, variables);
        if (!parentSlice) return nullptr;

        if (slice->empty() && !slice->getCondition()) {
            return parentSlice;
        }

        if (parentSlice->empty() && !parentSlice->getCondition()) {
            for (auto& child : parentSlice->getChildren()) {
                child->addChild(slice);
            }
        } else {
            parentSlice->addChild(slice);
        }

        return slice;
    }

    shared_ptr<Block> getPassHead(shared_ptr<Block> last_block) {
        assert(last_block);
        auto parent = last_block->getParent();
        if (!parent) return last_block;
        return getPassHead(parent);
    }

    set<Value*> extractDependenciesFromCondition(shared_ptr<Block> block) {
        set<Value*> variables;
        Value* condition = block->getCondition();
        if (condition) {
            // update variables set
            auto& addVars = variables_deps[condition];
            variables.insert(addVars.begin(), addVars.end());
            printVariableDependencies(std::cout, condition);
        } else {
            auto parent = block->getParent();
            if (parent && dynamic_pointer_cast<CallBlock>(parent)) {
                auto grangpa = parent->getParent();
                return extractDependenciesFromCondition(grangpa);
            } else {
                if (dynamic_pointer_cast<CallBlock>(block)) {
                    return extractDependenciesFromCondition(parent);
                }
            }
        }
        return variables;
    }

    shared_ptr<Block> sliceBlockByVariables(shared_ptr<Block> original_block, const set<Value*>& variables) {
        if (DEBUG_SLICER)
            cout << "slicing block: " << original_block->dump_instructions() << "\n";

        auto slice = make_shared<Block>(original_block->getCondition(), original_block->negateCondition());
        for (auto inst : original_block->getInstructions()) {
            if (StoreInst* store = dyn_cast<StoreInst>(inst)) {
                if (variables.count(store->getOperand(1))) {
                    slice->addInstruction(inst);
                }
            }
            else if (dyn_cast<BranchInst>(inst) || dyn_cast<AllocaInst>(inst)) {
                // skip
            }
            else {
                if (variables.count(inst)) {
                    slice->addInstruction(inst);
                }
            }
        }
        if (DEBUG_SLICER)
            cout << "sliced block: " << slice->dump_instructions() << "\n";
        return slice;
    }

    void find_dependencies(shared_ptr<Block> root, std::set<Block*> analysedBlocks = {}) {
        if (analysedBlocks.count(root.get())) return;
        analysedBlocks.insert(root.get());

        set<Value*> updateDeps;
        for (auto inst : root->getInstructions()) {
            grabDependencies(inst);
            updateDeps.insert(inst);
        }
//        update_dependencies(updateDeps);

        for (auto& child : root->getChildren()) {
            find_dependencies(child, analysedBlocks);
        }
    }

    void grabDependencies(Value* value) {
        Instruction* inst = dyn_cast<Instruction>(value);
        if (!inst) return;

        auto& current_mapper = variables_deps;
        if (StoreInst* store = dyn_cast<StoreInst>(inst)) {
            auto variable = store->getOperand(1);
            auto dependency = store->getOperand(0);
            makeDependency(variable, dependency);
            update_dependencies({variable, dependency});
        }
        else if (BranchInst* branch = dyn_cast<BranchInst>(inst)) {
            // skip, values are not changed in here
            if (branch->isConditional()) {
                auto condition = branch->getCondition();
                makeDependency(branch, condition);
                grabDependencies(condition);
            }
        }
        else if (AllocaInst *alloc = dyn_cast<AllocaInst>(inst)) {
            // skip - there is only align in operands
        }
        else if (CallInst *call = dyn_cast<CallInst>(inst)) {
            Value *variable = inst;
            for (unsigned int i = 0; i < call->getNumArgOperands(); i++) {
                Value *arg = call->getArgOperand(i);
                // only pointers can be changed in function calls
                if (isa<PointerType>(arg->getType())) {
                    makeDependency(arg, variable);
                }
                makeDependency(variable, arg);
            }
        }
        else {
            for (unsigned i = 0; i < inst->getNumOperands(); i++) {
                Value *dependency = inst->getOperand(i);
                makeDependency(inst, dependency);
//                if (isa<Constant>(dependency) || isa<CmpInst>(dependency) || isa<Instruction>(dependency)) {
//                    makeDependency(inst, dependency);
//                }
            }
        }
    }

    void makeDependency(Value* variable, Value* dependency) {
//        if (isa<Instruction>(variable) && isa<Instruction>(dependency)) {
        if (variables_deps.count(dependency)) {
            variables_deps[variable].insert(variables_deps[dependency].begin(), variables_deps[dependency].end());
        }
        variables_deps[variable].insert(dependency);
//        auto var_deps = variables_deps[variable];
//        for (auto dep : variables_deps[dependency]) {
//            dependent_variables[dep].insert(variable);
//            dependent_variables[dep].insert(var_deps.begin(), var_deps.end());
//        }
//        }
    }

    void update_dependencies(Value* variable) {
        update_dependencies({variable});
    }

    void update_dependencies(set<Value*> updated_deps) {
        while (!updated_deps.empty()) {
            Value* dependency = *updated_deps.begin();
            updated_deps.erase(dependency);
            if (variables_deps.count(dependency)) {
                auto add_deps = variables_deps[dependency];
                for (auto &pair : variables_deps) {
                    if (pair.second.count(dependency)) {
                        auto size = pair.second.size();
                        pair.second.insert(add_deps.begin(), add_deps.end());
                        if (size < pair.second.size()) updated_deps.insert(pair.first);
                    }
                }
            }
        }
    }

private:
    void printVariableDependencies(ostream& o, Value* value = nullptr) {
        if (!DEBUG_SLICER) return;

        if (value) {
            cout << value << " -> [";
            for (auto &dep : variables_deps[value]) {
                cout << "'" << dep << "',";
            }
            cout << "]" << std::endl;
        } else {
            for (auto &pair : variables_deps) {
                cout << pair.first << " -> [";
                for (auto &dep : pair.second) {
                    cout << "'" << dep << "',";
                }
                cout << "]" << std::endl;
            }
        }
    }

private:
    dependencies_map variables_deps;
    dependencies_map dependent_variables;
};
