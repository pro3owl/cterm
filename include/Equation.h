#pragma once

#include "Types.h"
#include "ValuesMapper.h"

template <class ValueType>
class Equation {
public:
    map<ValueType, int> arguments;
    pair<ValueType, int> next_state_var;
    ValueType state_var = nullptr;
    double free_arg = 0;
    shared_ptr<Equation<ValueType>> next_equation;

    void clone(const Equation& other) {
        arguments = other.arguments;
        free_arg = other.free_arg;
    }

    Equation& operator+=(const Equation& other) {
        free_arg += other.free_arg;
        for (auto& arg : other.arguments) {
            if (arguments.count(arg.first)) {
                arguments[arg.first] += arg.second;
            } else {
                arguments[arg.first] = arg.second;
            }
        }
        return *this;
    }

    Equation& operator-=(const Equation& other) {
        free_arg -= other.free_arg;
        for (auto& arg : other.arguments) {
            if (arguments.count(arg.first)) {
                arguments[arg.first] -= arg.second;
            } else {
                arguments[arg.first] = -arg.second;
            }
        }
        return *this;
    }

    Equation& operator*=(const Equation& other) {
        if (!(arguments.empty() || other.arguments.empty()))
            throw runtime_error("Variables mutation cannot be represented as linear equation");

        if (arguments.empty()) {
            int coef = free_arg;
            free_arg = other.free_arg * coef;
            for (auto& arg : other.arguments) {
                arguments[arg.first] = coef * arg.second;
            }
        } else {
            int coef = other.free_arg;
            free_arg *= coef;
            for (auto& arg : arguments) {
                arguments[arg.first] *= coef;
            }
        }
        return *this;
    }

    Equation makeLessThan(const Equation& other) {
        Equation equation;
        equation.clone(*this);
        equation -= other;
        equation.free_arg = -free_arg + other.free_arg;
        return equation;
    }

    virtual string dump(const ValuesMapper<ValueType>& mapper, const char* sign = 0) const {
        stringstream ss;
        bool is_empty = true;
        auto all_args = arguments;
        if (next_state_var.first) {
            assert(all_args.count(next_state_var.first) == 0);
            all_args[next_state_var.first] = next_state_var.second;
        }
        for (auto& argument : all_args) {
            if (is_empty) {
                is_empty = false;
                if (argument.second < 0)
                    ss << "- ";
            } else {
                if (argument.second < 0)
                    ss << " - ";
                else if (argument.second > 0)
                    ss << " + ";
            }
            if(argument.second != 0) {
                if (abs(argument.second) != 1)
                    ss << abs(argument.second) << "*";
                ss << mapper.getValueName(argument.first);
            }
        }

        if (!is_empty)
            ss << " " << (sign ? sign : (free_arg >= 0 ? " + " : " - "))
               << " " << (sign ? free_arg : abs(free_arg));
        else
            ss << free_arg;

//        if (next_equation) {
//            ss << ", " << next_equation->dump(mapper, is_inequality);
//        }
        return ss.str();
    }
};
