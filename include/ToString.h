#pragma once

#include "Types.h"

inline string predicate_to_str(CmpInst::Predicate predicate) {
    switch(predicate) {
        case CmpInst::Predicate::FCMP_FALSE: return "false";
        case CmpInst::Predicate::FCMP_TRUE: return "true";
        case CmpInst::Predicate::FCMP_OEQ: return "oredered ==";
        case CmpInst::Predicate::FCMP_OGT: return "oredered and >";
        case CmpInst::Predicate::FCMP_OGE: return "ordered and >=";
        case CmpInst::Predicate::FCMP_OLT: return "ordered and <";
        case CmpInst::Predicate::FCMP_OLE: return "ordered and <=";
        case CmpInst::Predicate::FCMP_ONE: return "ordered and !=";
        case CmpInst::Predicate::FCMP_ORD: return "ordered and not nans";
        case CmpInst::Predicate::FCMP_UNO: return "unordered: isnan(X) | isnan(Y)";
        case CmpInst::Predicate::FCMP_UEQ: return "unordered or ==";
        case CmpInst::Predicate::FCMP_UGT: return "unordered or >";
        case CmpInst::Predicate::FCMP_UGE: return "unordered or > or ==";
        case CmpInst::Predicate::FCMP_ULT: return "unordered or <";
        case CmpInst::Predicate::FCMP_ULE: return "unordered or < or ==";
        case CmpInst::Predicate::FCMP_UNE: return "unordered or !=";
        case CmpInst::Predicate::ICMP_EQ: return "==";
        case CmpInst::Predicate::ICMP_NE: return "!=";
        case CmpInst::Predicate::ICMP_UGT: return ">";
        case CmpInst::Predicate::ICMP_UGE: return ">=";
        case CmpInst::Predicate::ICMP_ULT: return "<";
        case CmpInst::Predicate::ICMP_ULE: return "<=";
        case CmpInst::Predicate::ICMP_SGT: return ">";
        case CmpInst::Predicate::ICMP_SGE: return ">=";
        case CmpInst::Predicate::ICMP_SLT: return "<";
        case CmpInst::Predicate::ICMP_SLE: return "<=";
        case CmpInst::Predicate::BAD_FCMP_PREDICATE:
        case CmpInst::Predicate::BAD_ICMP_PREDICATE:
        default:
            return "bad predicate";
    }
}

inline string typeid_to_string(Type::TypeID id) {
    switch(id) {
        case Type::TypeID::VoidTyID: return "void";
        case Type::TypeID::HalfTyID: return "float16";
        case Type::TypeID::FloatTyID: return "float32";
        case Type::TypeID::DoubleTyID: return "float64";
        case Type::TypeID::X86_FP80TyID: return "float80";
        case Type::TypeID::FP128TyID: return "float128";
        case Type::TypeID::PPC_FP128TyID: return "float128";
        case Type::TypeID::LabelTyID: return "label";
        case Type::TypeID::MetadataTyID: return "meta";
        case Type::TypeID::X86_MMXTyID: return "mmx vector";
        case Type::TypeID::IntegerTyID: return "int";
        case Type::TypeID::FunctionTyID: return "func";
        case Type::TypeID::StructTyID: return "struct";
        case Type::TypeID::ArrayTyID: return "array";
        case Type::TypeID::PointerTyID: return "pointer";
        case Type::TypeID::VectorTyID: return "vector";
        default: return "unknown";
    };
}

inline string solver_result_to_str(SolveResult res) {
    switch(res) {
        case SolveResult::SOLVE_RESULT_SAT: return "sat";
        case SolveResult::SOLVE_RESULT_UNSAT: return "unsat";
        case SolveResult::SOLVE_RESULT_UNKNOWN: return "unknown";
        case SolveResult::SOLVE_RESULT_ERROR: return "error";
        case SolveResult::SOLVE_RESULT_TIMEOUT: return "timeout";
    }
}
