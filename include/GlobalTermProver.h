#pragma once

#include "Types.h"
#include "Block.h"
#include "Slicer.h"
#include "TerminationProver.h"
#include "NonTerminationProver.h"
#include "RangerProver.h"
#include "SymbolicProver.h"
#include "DependenciesProver.h"
#include "NeverReturnsProver.h"

class GlobalTermProver {
public:
    GlobalTermProver()
    {
        nonterminationProvers.push_back(make_shared<DependenciesProver>());
        nonterminationProvers.push_back(make_shared<NeverReturnsProver>());
        terminationProvers.push_back(make_shared<RangerProver>());
//        terminationProvers.push_back(make_shared<SymbolicProver>());
    }

    void proveEachFunctionTermination(const vector<shared_ptr<FunctionBlock>>& functionBlocks) {
        for (auto block : functionBlocks) {
            if (block->isOnlyDeclaration()) continue;
            proveFunctionTermination(block);
        }
    }

private:
    void proveFunctionTermination(shared_ptr<FunctionBlock> function) {
        if (proves.count(function->getName()))
            return;

        if (proveNonTermination(function)) {
            std::cout << "функция '" << function->getName() << "' не завершается (доказано на этапе проверки незавершаемости)\n";
            functionDoesntTerminate(function);
        } else {
            if (proveTermination(function)) {
                std::cout << "функция '" << function->getName() << "' завершается (доказано на этапе проверки завершаемости)\n";
                functionTerminates(function);
            } else {
                std::cout << "функция '" << function->getName() << "' не завершается (доказано на этапе проверки завершаемости)\n";
                functionDoesntTerminate(function);
            }
        }
    }

    void functionDoesntTerminate(shared_ptr<FunctionBlock> function) {
        string name = function->getName();
        if (proves.count(name) && !proves[name]) return;

        proves[name] = false;
        for (auto& caller_info : function->getCallers()) {
            shared_ptr<FunctionBlock> caller = get<1>(caller_info);
            if (!proves.count(caller->getName()) || proves[caller->getName()] != false) {
                std::cout << "функция '" << caller->getName() << "' не завершается (из-за вызова функции '" << name << "')\n";
            }
            functionDoesntTerminate(caller);
        }
    }

    void functionTerminates(shared_ptr<FunctionBlock> function) {
        string name = function->getName();
        assert(!proves.count(name));
        proves[name] = true;
//        std::cout << "функция " << name << " завершается\n";
    }

    bool proveNonTermination(shared_ptr<FunctionBlock> function) {
        for (auto prover : nonterminationProvers) {
            auto res = prover->proveNonTermination(function, proves);
            bool proved;
            Location location;
            tie(proved, location) = res;
            if (proved) {
//                std::cout << "proved by " << prover->getName() << "\n";
                return true;
            }
        }
        return false;
    }

    bool proveTermination(shared_ptr<FunctionBlock> function) {
        bool function_terminates = false;
        for (auto prover : terminationProvers) {
            if (prover->proveTermination(function, proves)) {
                function_terminates = true;
//                std::cout << "proved by " << prover->getName() << "\n";
                break;
            }
        }
        return function_terminates;
    }

private:
    map<string, string> functionDependencies;
    map<string, bool> proves;
    vector<shared_ptr<TerminationProver>> terminationProvers;
    vector<shared_ptr<NonTerminationProver>> nonterminationProvers;
};
