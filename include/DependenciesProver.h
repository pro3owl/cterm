#pragma once

#include "NonTerminationProver.h"

class DependenciesProver : public NonTerminationProver {
public:
    tuple<bool, const Location> proveNonTermination(shared_ptr<FunctionBlock> function, map<string, bool>& functionTerminates) {
        bool terminates = true;
        for (auto outerCall : function->getOuterCalls()) {
            string called_func = outerCall->getCalledBlock()->getName();
            if (functionTerminates.count(called_func)) {
                terminates = terminates && functionTerminates[called_func];
            }
            if (!terminates) break;
        }
        return make_tuple(!terminates, function->getLocation());
    };

    string getName() { return "DependenciesProver"; }
};
