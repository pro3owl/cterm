#pragma once

#include <glpk.h>
#include "Types.h"
#include "ValuesMapper.h"
#include "Equation.h"

//#define DEBUG_LPSOLVER

template <typename ValueType>
class LPEquationsSolver {
public:
    LPEquationsSolver () {
    }

    bool solve(vector<Equation<ValueType>> equations) {
        if (equations.empty())
            return false;

        glp_init_env();
        glp_term_out(GLP_OFF);

        auto variables = extractAllVariables(equations);

        glp_prob *lp;
        int ia[1+1000], ja[1+1000];
        int ab_vars_count = equations.size() * 2;
        double ar[1+1000];
        lp = glp_create_prob();
        build_target_function(lp, equations, ia, ja, ar);

        int constr_index = 0;
        int ij_index = 0;

        // a * A' = 0
        for (auto &variable : variables) {
            glp_add_rows(lp, 1); constr_index++;
            glp_set_row_bnds(lp, constr_index, GLP_FX, 0.0, 0.0);
            for (unsigned int i = 0; i < equations.size(); i++) {
                auto& inequality = equations[i];
                int avar_index = equations.size() + i + 1;
                double argument = inequality.state_var == variable ? inequality.next_state_var.second : 0;
                ij_index++;
                ia[ij_index] = constr_index, ja[ij_index] = avar_index, ar[ij_index] = argument;
#ifdef DEBUG_LPSOLVER
                std::cout << "a" << i << " * " << argument << " = 0\n";
#endif
            }
        }

        // (a - b) * A = 0
        for (auto &variable : variables) {
            glp_add_rows(lp, 1); constr_index++;
            glp_set_row_bnds(lp, constr_index, GLP_FX, 0.0, 0.0);
            for (unsigned int i = 0; i < equations.size(); i++) {
                auto& inequality = equations[i];
                int avar_index = equations.size() + i + 1;
                int bvar_index = i + 1;
                double argument = inequality.arguments[variable];
                ij_index++;
                ia[ij_index] = constr_index, ja[ij_index] = avar_index, ar[ij_index] = argument;
                ij_index++;
                ia[ij_index] = constr_index, ja[ij_index] = bvar_index, ar[ij_index] = -argument;
            }
        }

        // b * (A + A') = 0
        for (auto &variable : variables) {
            glp_add_rows(lp, 1); constr_index++;
            glp_set_row_bnds(lp, constr_index, GLP_FX, 0.0, 0.0);
            for (unsigned int i = 0; i < equations.size(); i++) {
                auto& inequality = equations[i];
                int bvar_index = i + 1;
                double argument = inequality.arguments[variable];
                if (inequality.state_var == variable) {
                    argument += inequality.next_state_var.second;
                }

                ij_index++;
                ia[ij_index] = constr_index, ja[ij_index] = bvar_index, ar[ij_index] = argument;
            }
        }

        // b * free_args < 0
        int fake_var = glp_add_cols(lp, 1);
        glp_set_col_bnds(lp, fake_var, GLP_LO, 0.001, 0.0);

        glp_add_rows(lp, 1); constr_index++;
        glp_set_row_bnds(lp, constr_index, GLP_FX, 0.0, 0.0);
        ij_index++;
        ia[ij_index] = constr_index, ja[ij_index] = fake_var, ar[ij_index] = 1;
        for (int i = 0; i < ab_vars_count / 2; i++) {
            int bvar_index = i + 1;
            double argument = static_cast<double>(equations[i].free_arg);
            ij_index++;
            ia[ij_index] = constr_index, ja[ij_index] = bvar_index, ar[ij_index] = argument;
        }

        glp_load_matrix(lp, ij_index, ia, ja, ar);
        glp_simplex(lp, NULL);
        print_result(lp, ab_vars_count);
        int solve_status = glp_get_status(lp);
        glp_delete_prob(lp);
        glp_free_env();
        return solve_status == GLP_OPT;
    }

private:
    void build_target_function(glp_prob* lp, vector<Equation<ValueType>> equations, int* ia, int* ja, double* ar) {
        int ab_vars_count = equations.size() * 2;
        glp_set_obj_dir(lp, GLP_MAX);
        glp_add_cols(lp, ab_vars_count);
        for (int i = 0; i < ab_vars_count; i++) {
            int index = i + 1;
            // all >= 0
            glp_set_col_bnds(lp, index, GLP_LO, 0.0, 0.0);
            if (i < ab_vars_count / 2) {
                // lambda_2 (b)
                glp_set_col_name(lp, index, "b");
                // b * free_args
                double argument = static_cast<double>(equations[i].free_arg);
                glp_set_obj_coef(lp, index, argument);
            } else {
                // lambda_1 (a)
                glp_set_col_name(lp, index, "a");
                glp_set_obj_coef(lp, index, 0.0);
            }
        }
    }

    void print_result(glp_prob* lp, int ab_vars_count) {
#ifdef DEBUG_LPSOLVER
        double z = glp_get_obj_val(lp);
        cout << "f=" << z << "\n";
        for (int i = 0; i < ab_vars_count; i++) {
            int index = i + 1;
            if (i < ab_vars_count / 2) {
                cout << "b" << i << " = ";
            }
            else {
                cout << "a" << (i % (ab_vars_count / 2)) << " = ";
            }
            cout << glp_get_col_prim(lp, index) << "\n";
        }
        cout << "fake_var=" << glp_get_col_prim(lp, ab_vars_count + 1) << "\n";
#endif
    }

    set<ValueType> extractAllVariables(const vector<Equation<ValueType>>& equations) {
        set<ValueType> variables;
        for (auto& inequality : equations) {
            for (auto& arg : inequality.arguments) {
                variables.insert(arg.first);
            }
        }
        return variables;
    }
};
