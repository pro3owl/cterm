#pragma once

#include "Types.h"
#include "Block.h"
#include "GlobalTermProver.h"

class ControlFollower {
private:
    vector<shared_ptr<FunctionBlock>> functionBlocks;
    string filename;
    std::map<BasicBlock*, shared_ptr<Block>> analyzedBlocks;
    shared_ptr<FunctionBlock> currentFunction;
    std::set<BasicBlock*> callStack;

public:
    void on_global_variables(Module::GlobalListType& globals) {
    }

    void on_module_end() {
        GlobalTermProver prover;
        prover.proveEachFunctionTermination(functionBlocks);
        functionBlocks.clear();
    }

    void on_function(Function &func, string module_path) {

        filename = module_path;
        auto function_name = func.getName().str();
        auto root = find_or_create_function_block(func);
        if (!root->empty()) {
            return;
        }

        currentFunction = root;
        on_block(&func.getEntryBlock(), root);

        analyzedBlocks.clear();
        callStack.clear();
        currentFunction = nullptr;
    }

    shared_ptr<FunctionBlock> find_or_create_function_block(Function& func) {
        auto function_name = func.getName().str();
        for (auto block : functionBlocks) {
            if (block->getName() == function_name)
                return block;
        }

        auto root = make_shared<FunctionBlock>(function_name, &func);
        root->updateLocation(filename);
        functionBlocks.push_back(root);
        for (auto& arg : func.getArgumentList()) root->addArg(&arg);
        if (func.isDeclaration())
            root->setOnlyDeclaration();
        return root;
    }

    void on_block(BasicBlock* block, shared_ptr<Block> root, Value* condition = nullptr, bool negate = false) {
        assert(root);
        if (block->getInstList().empty()) return;
        if (analyzedBlocks.count(block)) {
            auto child_block = analyzedBlocks[block];
            root->addChild(child_block);
            if (callStack.count(block))
                analyzedBlocks[block]->addAlternativeParent(root, condition, negate);
            return;
        }
        callStack.insert(block);

        auto child_block = make_shared<Block>(condition, negate);
        root->addChild(child_block);
        root = child_block;
        analyzedBlocks[block] = root;

        auto current_block = root;
        for (auto& inst : *block) {
            if (isa<CallInst>(inst)) {
                auto& call = cast<CallInst>(inst);
                auto raw_function = find_or_create_function_block(*call.getCalledFunction());
                auto call_block = make_shared<CallBlock>(raw_function);
                if (currentFunction != raw_function) {
                    currentFunction->addOuterCall(call_block);
                    raw_function->addCaller(make_tuple(call_block, currentFunction));
                } else {
                    raw_function->addCall(call_block);
                }
                for (unsigned i = 0; i < call.getNumArgOperands(); i++) {
                    auto var = call.getArgOperand(i);
                    call_block->addVariable(var);
                }
                call_block->addInstruction(&inst);
                current_block->addChild(call_block);
                auto temp = make_shared<Block>();
                call_block->addChild(temp);
                current_block = temp;
                continue;
            }
            // it should be only last instruction actually
            if (isa<BranchInst>(inst)) {
                auto& branch = cast<BranchInst>(inst);
                on_branch(branch, current_block);
                continue;
            }

            current_block->addInstruction(&inst);
            if (isa<ReturnInst>(inst)) {
                current_block->setContainsReturn();
            }
        }

        callStack.erase(block);
    }

    void on_branch(BranchInst &branch, shared_ptr<Block> root) {
        assert(branch.getNumSuccessors() <= 2);

        if (branch.isUnconditional()) {
            // unconditional;
            auto block = branch.getSuccessor(0);
            on_block(block, root);
            return;
        }

        Value* condition_then = branch.getCondition();

        auto block_then = branch.getSuccessor(0);
        on_block(block_then, root, condition_then);

        auto block_else = branch.getSuccessor(1);
        on_block(block_else, root, condition_then, true);
    }
};
