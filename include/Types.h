#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <utility>
#include <set>
#include <memory>
#include <cassert>
#include <cmath>

#include <json/json.h>

#include <sonolar/sonolar.hpp>

using namespace std;

using SONOLAR::SolveResult;
using SONOLAR::Solver;
using SONOLAR::Expression;
