#pragma once

#include "Types.h"
#include "Block.h"
#include "Slicer.h"

class TerminationProver {
public:
    virtual ~TerminationProver() {};

    virtual bool proveTermination(shared_ptr<FunctionBlock> function, map<string, bool>& functionTerminates)  = 0;
    virtual string getName() = 0;
};

