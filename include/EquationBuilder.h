#pragma once

#include "ToString.h"
#include "Equation.h"
#include "ValuesExpressions.h"

typedef Equation<Value*> InstructionEquation;

class EquationBuilder {
public:
    static vector<InstructionEquation> makeMutationInequality(InstructionEquation statement, Value* var,
                                                              Value* new_var, shared_ptr<ValuesMapper<Value*>> mapper) {
        // if new_var doesnt depend on var
        if (statement.arguments.count(var) == 0) {
//            cerr << "new var doesnt depend on var\n";
            return {};
        }

        // if new_var == var
        if (statement.arguments.size() == 1 && statement.arguments[var] == 1 && statement.free_arg == 0) {
//            cerr << "variable doesnt change\n";
            return {};
        }

        vector<InstructionEquation> inequalities;
        // if new_var >= var
        if (check_greater_equal(statement, var, mapper))
        {
            InstructionEquation inequality;
            inequality.free_arg = -statement.free_arg;
            inequality.arguments = statement.arguments;
            inequality.next_state_var = {new_var, -1};
            inequality.state_var = var;
            inequalities.push_back(inequality);
        };

        // if new_var <= var
        if (check_less_equal(statement, var, mapper)) {
            InstructionEquation inequality;
            inequality.free_arg = statement.free_arg;
            for (auto arg : statement.arguments) {
                inequality.arguments[arg.first] = -arg.second;
            }
            inequality.next_state_var = {new_var, 1};
            inequality.state_var = var;
            inequalities.push_back(inequality);
        }

        return inequalities;
    }

    static vector<InstructionEquation> makeLimitsInequalities(Value* var) {
        IntegerType* type = getIntType(var);
        if (!type) {
            std::cerr << "Ошибка: тип " << type << " переменной " << var << " не является целочисленным\n";
            return {};
        }
        return { makeMinValueInequality(var), makeMaxValueInequality(var) };
    }

    static IntegerType* getIntType(Value* var) {
        IntegerType* type = dyn_cast<IntegerType>(var->getType());
        if (!type) {
            PointerType* pointer_type = dyn_cast<PointerType>(var->getType());
            if (pointer_type) {
                type = dyn_cast<IntegerType>(pointer_type->getElementType());
            }
            if (!type) {
                return nullptr;
            }
        }
        return type;
    }

    static InstructionEquation makeMinValueInequality(Value* var) {
        InstructionEquation min_val;
        min_val.free_arg = -getMinPossibleValue(var);
        min_val.arguments[var] = -1;
        return min_val;
    }

    static InstructionEquation makeMaxValueInequality(Value* var) {
        InstructionEquation max_val;
        max_val.free_arg = getMaxPossibleValue(var);
        max_val.arguments[var] = 1;
        return max_val;
    }

    static double getMinPossibleValue(Value* var) {
        IntegerType* type = getIntType(var);
        unsigned bitwidth = type->getIntegerBitWidth();
        if (type->getSignBit()) {
            return - (1 << (bitwidth - 1));
        } else {
            return 0;
        }
    }

    static double getMaxPossibleValue(Value* var) {
        IntegerType* type = getIntType(var);
        unsigned bitwidth = type->getIntegerBitWidth();
        if (type->getSignBit()) {
            return (1 << (bitwidth - 1)) - 1;
        } else {
            return 1 << bitwidth;
        }
    }

    static double getValueRange(Value* var) {
        IntegerType* type = getIntType(var);
        unsigned bitwidth = type->getIntegerBitWidth();
        double range = 1L << bitwidth;
        return range;
    }

    static InstructionEquation makeVar(Value* var) {
        InstructionEquation equal;
        equal.free_arg = 0;
        equal.arguments[var] = 1;
        return equal;
    }

    static bool check_greater_equal(InstructionEquation statement, Value* var, shared_ptr<ValuesMapper<Value*>> mapper) {
        // @todo: get max bitwidth
        ValuesExpressions expr_mapper(32);
        Solver& solver = expr_mapper.getSolver();
        Expression update = get_expression(statement, mapper, expr_mapper);
        Expression var_expr = expr_mapper.getValueExpression(var);

        solver.assert_formula(solver.make_sge(update, var_expr));
        auto result = solver.solve();
        return result == SONOLAR::SOLVE_RESULT_SAT;
    }

    static bool check_less_equal(InstructionEquation statement, Value* var, shared_ptr<ValuesMapper<Value*>> mapper) {
        // @todo: get max bitwidth
        ValuesExpressions expr_mapper(32);
        Solver& solver = expr_mapper.getSolver();
        Expression update = get_expression(statement, mapper, expr_mapper);
        Expression var_expr = expr_mapper.getValueExpression(var);

        solver.assert_formula(solver.make_sle(update, var_expr));
        auto result = solver.solve();
        return result == SONOLAR::SOLVE_RESULT_SAT;
    }

    static Expression get_expression(InstructionEquation statement, shared_ptr<ValuesMapper<Value*>> mapper,
                              ValuesExpressions& expr_mapper) {
        Expression expression = expr_mapper.getConstantExpression(static_cast<int>(statement.free_arg));
        Solver& solver = expr_mapper.getSolver();
        for (auto& arg : statement.arguments) {
            Expression coef = expr_mapper.getConstantExpression(arg.second);
            Expression variable = expr_mapper.getValueExpression(arg.first);
            Expression participent = solver.make_mul(coef, variable);
            solver.assert_formula(solver.make_not(solver.make_smul_ovfl(coef, variable)));
            solver.assert_formula(solver.make_not(solver.make_sadd_ovfl(participent, expression)));
            expression = solver.make_add(participent, expression);
        }
        return expression;
    }

    static InstructionEquation makePositiveOverflow(InstructionEquation original) {
        InstructionEquation equation = original;
        equation.free_arg -= getValueRange(original.state_var);
        return equation;
    }

    static InstructionEquation makeNegativeOverflow(InstructionEquation original) {
        InstructionEquation equation = original;
        equation.free_arg += getValueRange(original.state_var);
        return equation;
    }
};
